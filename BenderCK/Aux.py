#!/usr/bin/env python


#==============================================================================

## Hack for less verbose printing
# - Quicker to call syntax like: log.info('Prefix'+p)
# - Align the same for p/mcp

def MCParticle__str__( old_method ):
  """
  Helper hack to better align the whitespace from str(MCParticle)
  """
  def wrap(self):
    return old_method(self).replace(' '*25,' '*10) #.strip()
  return wrap

def Particle__add__( old_method ):
  """
  Helper hack to allow implicit casting to string, when Particle is add with basestring instance.
  Useful for debugging purpose
  """
  def wrap( self, other ):
    if isinstance( other, basestring ):
      return str(self).strip('\n') + other 
    return old_method( self, other )
  return wrap

def Particle__radd__( old_method ):
  def wrap( self, other ):
    if isinstance( other, basestring ):
      return other + str(self).strip('\n')
    return old_method( self, other )
  return wrap
