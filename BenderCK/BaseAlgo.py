"""
Helper classes & methods that can be used inside LHCb's Bender
"""

# from . import *

import os
from . import (
  std, LHCb, Algo, AlgoMC, logger, Functions,
  ALL, EXISTS, CONTAINS, MCTRUTH, MC3Q, Q, ID
)
from .Decorators import static_algo_property

#==============================================================================

Std_Vector_Particle = std.vector('const LHCb::Particle*')
def std_vector_particle(iterable):
  v = Std_Vector_Particle()
  v += list(iterable)
  return v


def LHCb__Particle__ConstVector(rawcontainer):
  res = LHCb.Particle.ConstVector()
  if rawcontainer:
    res += rawcontainer
  return res


# A-la-decorator
def algo_select(old_method):
  """Enhanced Algo.select statement."""

  def wrap(self, name, *args, **kwargs):

    def is_list():
      return len(args)>=1 and not kwargs and isinstance(args[0], list)
    # def is_string(startswith):
    #   return len(args)>=1 and not kwargs and isinstance(args[0], basestring) and args[0].startswith(startswith) 

    # Allow it to accept the pythonic list via std.vector<LHCb::Particle*>
    if is_list():
      cont = std_vector_particle(args[0])
      cuts = ALL if len(args)==1 else args[1]
      return old_method(self, name, cont, cuts )

    return old_method(self, name, *args, **kwargs)
  return wrap


# def _algo_selectTES(self, TESname): 
#   """
#   Shortcut method to select particles from TES location 

#   Caveats:
#   - The location NEEDS to be declared at constructors.
#   - Cannot accept additional cuts, in order to reuse same name with caching.
#     This impose a restriction with trade for simplicity of usage.
#   - Shortname can be use (implicit '/Event', '/Event/Phys' from INTES)
#   - For newly-creates TES, checkout the flag `CloneFilteredParticles=True`
#   """
  
#   if not TESname.endswith('/Particles'):
#     TESname += '/Particles'
#   val = self.selected(TESname)
#   if val.size()==0:  # Cache will not work if range is truly empty
#     val = self.select(TESname, INTES(TESname))
#     logger.debug('Loaded new selectTES: {:30}. size={}'.format(TESname,val.size()))
#   return val

def _algo_selectTES(self, TESname):
  """
  Variant of Algo.select, which return container of Particle.
  Null-container will still support iteration having len==0, unlike default which 
  will raise and have to be guarded.

  The prefix 'Phys' and suffix 'Particles' are implicit.
  """
  if not TESname.endswith('/Particles'):
    TESname += '/Particles'
  queue = [ TESname, os.path.join('Phys',TESname) ]
  # rit = getattr( self, 'RootInTES', '' ) ## Need better understanding first.
  # if rit:
  #   queue += [ os.path.join(rit,TESname), os.path.join(rit,'Phys',TESname)  ]
  ## Now loop over each fallback searchpath.
  for cname in queue:
    l = self.get(TESname)
    if l:
      return l 
  ## Final, default to null-list
  return []
  
  #   l = 
  # ## 1. Raw
  # if l: return l 
  # ## 2. Fallback with 'Phys/' prefix
  # if not TESname.startswith('Phys/'):
  #   l = self.get('Phys/'+TESname)
  #   if l: return l
  # ## Null
  # return []

#==============================================================================

class NewBaseAlgo(Algo):
  
  def __init__(self, name=None, *args, **kwargs):
    """Provide lazy constructor with default naming. Be careful of name-collision yourself."""
    ## Lazy naming
    if not name:
      name = self.__class__.__name__.replace('Algo','')

    ## Lazy bypass if class-attribute `Inputs` is presents inside the class.
    # http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/davinci/releases/v37r0/doxygen/d2/d09/class_d_v_common_base.html
    ## TODO: Find these declareProperty automatically
    for key in [ 'Inputs', 'OutputLevel', 'P2PVInputLocations', 'IgnoreP2PVFromInputLocations', 'RootInTES' ]:
      if hasattr( self, key ) and key not in kwargs: ## If provided in class, but not explicitly in constructor
        kwargs[key] = getattr( self, key )

    ## Init
    super(NewBaseAlgo, self).__init__(name, *args, **kwargs)
    ## Expand counter by default
    self.StatEntityList = [ ".*" ]

  def selectTES(self, *args, **kwargs):
    return _algo_selectTES(self, *args, **kwargs)

class NewBaseAlgoMC(AlgoMC):
  """

  Inherited useful interfaces automatically.
  - MCReconstructible
  - MCReconstructed

  Feature:
  - Alternative passing of `Inputs` arguments to constructor.
    Normally, this flag (Inputs=['Phys/StdAllLooseMuons']) is passed at the
    constructor level. Alternatively, it can be hard-coded in the __init__ 
    method, but this eventually accompanied by lots of boilerplates

    Instead, this class support another approach, by marking class-level
    attribute `Inputs`, it'll be loaded automatically

    Usage:
    >> class MyClass(AlgoMC):
    >>   Inputs = [ 'Phys/StdAllLooseMuons' ]
    >>   ...

  """

  def __init__(self, name=None, *args, **kwargs):
    """Provide lazy constructor with default naming. Be careful of name-collision yourself."""
    ## Lazy naming
    if not name:
      name = self.__class__.__name__.replace('AlgoMC','')

    ## Lazy bypass if class-attribute `Inputs` is presents inside the class.
    # http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/davinci/releases/v37r0/doxygen/d2/d09/class_d_v_common_base.html
    ## TODO: Find these declareProperty automatically
    for key in [ 'Inputs', 'OutputLevel', 'P2PVInputLocations', 'IgnoreP2PVFromInputLocations', 'RootInTES' ]:
      if hasattr( self, key ) and key not in kwargs: ## If provided in class, but not explicitly in constructor
        kwargs[key] = getattr( self, key )

    ## Init
    super(NewBaseAlgoMC, self).__init__(name, *args, **kwargs)
    ## Expand counter by default
    self.StatEntityList = [ ".*" ]
    ## Run-evt cache
    self._runevt_cache = {}
    self._runevt_last  = None


  def selectTES(self, *args, **kwargs):
    return _algo_selectTES(self, *args, **kwargs)

  # ## IMCReconstructible
  # @static_algo_property
  # def _MCReconstructible(self):
  #   return self.tool('IMCReconstructible', 'MCReconstructible')
  # def reconstructible(self, mcp):
  #   # Is better to typecast them to simplest types for safety
  #   return int(self._MCReconstructible.reconstructible(mcp))
  # def inAcceptance(self, mcp):
  #   return bool(self._MCReconstructible.inAcceptance(mcp))
  # def isReconstructibleAs(self, *args, **kwargs):
  #   return self._MCReconstructible.isReconstructibleAs(*args, **kwargs)
  # def isReconstructible(self, mcp):
  #   return self.reconstructible(mcp) >= 2

  # ## IMCReconstructed
  # @static_algo_property
  # def _MCReconstructed(self):
  #   return self.tool('IMCReconstructed', 'MCReconstructed')
  # def reconstructed(self, mcp):
  #   return int(self._MCReconstructed.reconstructed(mcp))
  # def isReconstructed(self, mcp):
  #   return self.reconstructed(mcp) > 0

  @static_algo_property  # Should be fixed for given dataset, so cache me.
  def is_MC(self):
    return EXISTS('MC/Particles')() and CONTAINS('MC/Particles')()>0

  @property
  def runevt(self):
    """
    Return tuple of (runnumber, evtnumber), useful as index key for
    many other caching-decorators.
    """
    header  = self.get('Rec/Header')
    if not header:
      header = self.get('Gen/Header')  # Alternative for non-reco sample
    return (header.runNumber(), header.evtNumber())

  @property
  def event_number(self):
    return self.runevt[1]

  @property
  def event_break(self):
    """Return string to be printted as event-break (like page-break)"""
    return '='*40+'[ Event: run:{} evt:{} ]'.format(*self.runevt)+'='*40

  @property
  def event_type(self):
    """
    Note: Not static: I can run this over multi-process job.
    """
    return int(self.get('Gen/Header').evType()) if self.is_MC else 0


  #-----------------------------------------------------------------

  @property
  def runevt_cache(self):
    runevt = self.runevt
    if self._runevt_last != runevt:
      self._runevt_last  = runevt
      self._runevt_cache = {}  # reset
    return self._runevt_cache
  
  #-----------------------------------------------------------------

  import contextlib

  @contextlib.contextmanager
  def enhancedTuple(self, tuplefullpath, auto_write=True):
    """
    USAGE:
    >>> with self.enhancedTuple('tuplename') as tup:
    ...   tup.column('key', val)                    # as usual
    ...   tup.RecoStats.fill()                      # Lazy init IEventTupleTool, tool will be cached singleton-like.
    ...   tup.RecoStats.fill(Verbose=True)          # Extra flag goes into kwargs.
    ...   tup.fill_dict(p, dict, prefix='prefix_')  # Dictionary

          ## TODO
          tup.Kinematic.fill(p, prefix='prefix_')   # Lazy init IParticleTupleTool

          # Upon exit, tup.write() will be called
    """
    tname = tuplefullpath.replace('/','__') # To still guarantee unique name by path
    try:
      # V1. Normal
      tup = self.nTuple(tuplefullpath, tname)
      tup._algo = self

      # cache for tuple's funcdict
      if not hasattr(self, '_etfd'):
        self._etfd = {}

      # V2. Cache the entire tuple (perhaps memleak here?)
      # ## Need for speed
      # key = '__enhancedtup_'+tuplename
      # tup = self.__dict__.get(key, None)
      # if tup is None:
      #   tup = self.nTuple(tuplename)
      #   tup._algo = self
      #   self.__dict__[key] = tup 

      yield tup
    finally:
      tup._algo = None
      if auto_write:
        tup.write()
      del tup

  #-----------------------------------------------------------------------------

  def find_reco_all( self, tes, mcp, find_similar=False ):
    """
    Helper method: Give a mcp, return list of reco particles matches in given TES.
    No sort is performed.

    If find_similar = True, turn on the search in given TES for particle of 
    similar 4-vector norm & dir, only if that reco has no matching mcp.
    Use this flag with care, since it can disturb the definition of tracking 
    efficiency and kinematic efficiency.
    """
    matcher = MCTRUTH( mcp, self.mcTruth() )
    for cand in self.selectTES( tes ):
      ## 1. Search with MCTRUTH
      if matcher(cand):
        yield cand
      ## 2. (Optional) search with 4-vector similarity
      elif find_similar and Functions.psimilar(mcp, cand):
        # please at least have same-sign
        if MC3Q(mcp)==3*Q(cand):
          mcp2 = self.p2mc(cand)
          logger.debug('find similar: MCP  = %r'%mcp)
          logger.debug('find similar: reco = %r'%cand)
          logger.debug('find similar: MCP2 = %r'%mcp2)
          if not mcp2: 
            yield cand


  # def find_reco_best(self, tes, mcp):
  #   """
  #   Extention of `find_reco_all`, but return single LHCb.Particle considered as 
  #   best match from above.
  #   Return None if the matching fails.
  #   """
  #   l = self.find_reco_all(tes, mcp)
  #   ## Abort if not found
  #   if not l:
  #     return None
  #   ## Let's start with those of same ID first, sort by size closest to original's
  #   l1 = [ p for p in l if ID(p)==MCID(mcp) ]
  #   if l1:
  #     return l1[0]
  #   ## Else, the closest match
  #   return l[0] 

  @static_algo_property
  def p2mc(self):
    return self.tool('IP2MCP', 'MCMatchObjP2MCRelator')

  def find_mcp_all( self, reco, find_similar=True ):
    """
    Do the opposite of above, given a reco, find its best mcp, including 4-vect way.
    """
    ## 1. From the IP2MCP
    mcp1 = self.p2mc(reco)
    if mcp1:
      yield mcp1
    ## 2. From vectorial similarity
    if find_similar:
      for mcp2 in self.get('MC/Particles'):
        if Functions.psimilar(mcp2, reco) and mcp2!=mcp1:
          if MC3Q(mcp2)==3*Q(reco):
            yield mcp2

  def find_mcp_best( self, reco, find_similar=True ):
    """
    Return None when search failed.
    """
    res = list(self.find_mcp_all(reco,find_similar))
    if not res:
      return None
    ## Sort by closest to original's
    return sorted(res, key=lambda mcp: Functions.variance(mcp,reco))[0]


  def combine(self, pid, *particles):
    """
    Wrap functionality of combine particle.
    """
    ## dummy mom & vertex output
    mother   = LHCb.Particle(LHCb.ParticleID(int(pid)))
    vertex   = LHCb.Vertex() # output vertex
    ## Wrap to vector
    children = LHCb__Particle__ConstVector(particles)
    ## If any of the children is bad, early abort & return null particle
    if not all( (bool(p) and ID(p)) for p in children ):
      self.Warning('Bad daughters input, abort')
      # self.Warning(str(children)) # this should be debug instead
      mother.setEndVertex(vertex)
      return mother
    ## Start combining
    combiner = self.particleCombiner()
    sc       = combiner.combine( children, mother, vertex )
    if sc.isFailure():
      self.Warning("CombineParticle failed")
      # self.Warning(str(children))
      mother.setEndVertex(vertex)
      return mother
    ## Good result.
    return mother


#==============================================================================
# BINDING
#==============================================================================

# try:
#   # cpp.Tuples.Tuple.put = tuple_put
#   # cpp.Tuples.Tuple.__getattr__ = _tuple__getattr__
#   # cpp.Tuples.Tuple.fill_dict   = _tuple_fill_dict
#   # cpp.Tuples.Tuple.fill_gen    = _tuple_fill_gen

#   # Algo.select       = algo_select(Algo.select)
#   # AlgoMC.select     = algo_select(AlgoMC.select)
#   # AlgoMC.find_recon = find_recon

#   # _old_Tuple = cpp.Tuples.Tuple
#   # cpp.Tuples.Tuple = NewBaseTuple

#   # _old_Algo   = Algo 
#   # _old_AlgoMC = AlgoMC 
#   # Algo   = NewBaseAlgo
#   # AlgoMC = NewBaseAlgoMC
  
# except ImportError:
#   logger.warning("Warning, Bender.Main not available.")


#==============================================================================
