
import os
import sys
from PythonCK.itertools import flatten
from . import logger, importOptions, appMgr

def _maybe_mc(uri):
  if '/MC/' in uri:
    return True
  return False

def _maybe_real(uri):
  if '/lhcb/LHCb/' in uri:
    return True 
  if '/Collision' in uri:
    return True
  return False

def guess_single_data_ISMC(uri):
  """
  Given one source of data, guess whether it's MC or not.
  """
  logger.info('Parsing: '+uri)
  has_mc = _maybe_mc(uri)
  has_re = _maybe_real(uri)
  if has_mc==has_re:
    logger.warning('Unable to guess data type: %s.'%uri)
    logger.warning('Will use MC=True by default, in absentia.')
    has_mc = True
  return has_mc  

def guess_data_ISMC(*list_uri):
  """
  Like above, with check that all uri return the same guess.
  """
  ## flatten first
  list_uri = flatten(list_uri)
  ## Loop check.
  ISMC = None 
  for uri in list_uri:
    ismc = guess_single_data_ISMC(uri)
    if ISMC is not None and (ISMC != ismc):
      raise IOError('Conflicting data type in one job, Abort!!:\n'+uri)
  return ISMC

# def setdata_and_configure( inputdata , catalogs , castor, *list_options, **kwargs ):
#   """
#   Equivalent to `setData` + `simple_configure`, with helper method to determine 
#   data type from uri.
#   """
#   ## Equip inputdata to list
#   if isinstance(inputdata, basestring):
#     inputdata = [ inputdata ]

#   ## Use explicit IS_MC type, if given
#   IS_MC = kwargs.get('IS_MC', None)

#   ## Try to determine implicitly otherwise
#   if IS_MC is None:
#     for dat in inputdata:
#       has_mc = _maybe_mc(dat)
#       has_re = _maybe_real(dat)
#       if has_mc==has_re:
#         logger.warning('Unable to guess data type: %s. Specify implicitly with `IS_MC` kwarg into `set_and_configure`'%dat)
#         logger.warning('Will use MC=True by default, in absentia.')
#         has_mc = True
#       if IS_MC is not None and (IS_MC != has_mc):
#         raise IOError('Conflicting data type in one job, Abort!!:\n'+dat)
#       logger.info('Parsing: '+dat)
#       IS_MC = has_mc  # Establish the implicit flag
#   logger.info('IS_MC: %r'%IS_MC)

#   ## Finally, set the data
#   # setData( inputdata , catalogs , castor )  

#   ## pass baton to simple_configure
#   return simple_configure( IS_MC, *list_options )


def simple_configure( IS_MC, *list_options ):
  """
  Leverage the fact that LHCb Configurable works in a singleton manner...
  Return a Sequencer appended to main algo.

  NOTE: There's no `setData( inputdata , catalogs , castor )` !! Call it yourself!

  Usages:
      ### Only if without pre-gaudi algo
      seq = simple_configure(True)
      seq.Members += [ BenderAlgo('algo1').name() ]
  """
  
  ### XML
  from Configurables import LHCbApp
  LHCbApp().XMLSummary = 'summary.xml'

  ### Msg
  # Default: % F%18W%S%7W%R%T %0W%M
  # F: (fill)
  # W: (width)
  # S: Source
  # R: (justify right)
  # T: Type of log
  # M: Message
  from Configurables import MessageSvc
  MessageSvc().Format = "% F%32W%S%7W%R%T %0W%M"

  # from Configurables import HistogramSvc
  # HistogramSvc( 'HistogramDataSvc' )

  # from Configurables import MessageSvc
  # MessageSvc().OutputLevel = 0
  # from Configurables import ApplicationMgr
  # ApplicationMgr().OutputLevel = 0
  # ApplicationMgr().PluginDebugLevel = 1

  # ## For memory/chrono audit
  # from Configurables import AuditorSvc
  # AuditorSvc().Auditors += [ 'ChronoAuditor' ]
  # AuditorSvc().Auditors += [ 'MemStatAuditor' ]

  ## DaVinci.
  # Prepare default value before user's options
  from Configurables import DaVinci
  DaVinci().TupleFile       = 'tuple.root'
  DaVinci().HistogramFile   = 'hist.root'
  DaVinci().InputType       = 'DST'
  DaVinci().DataType        = '2012'
  DaVinci().Lumi            = not IS_MC
  DaVinci().Simulation      = IS_MC

  ## Add the list options here. After default, but before mySeq
  for opt in list_options:
    ## Warning if path is non-absolute
    opt = os.path.expandvars(opt)
    if not os.path.isabs(opt):
      logger.warning('For safety please use abspath: '+opt)
    else:
      logger.info('Importing: '+opt)
    ## Identify existence first
    exists = os.path.exists(opt)
    if not exists: # fallback to local in first fail
      opt = os.path.split(opt)[-1]
      logger.info('Import failed, fallback to local import: '+opt)
      exists = os.path.exists(opt)
    if not exists:   # Both fail, abort immediately, let user checkme.
      logger.error('Failed to import opt, abort')
      sys.exit(-1)
    ## Then, begin import
    importOptions(opt)

  ## Prepare the sequence
  from Configurables import GaudiSequencer
  seq_name = 'MyBenderSequence'
  DaVinci().UserAlgorithms += [ GaudiSequencer(seq_name) ]
  # DaVinci().appendToMainSequence([ GaudiSequencer(seq_name) ])

  # Ignite!
  gaudi = appMgr()
  gaudi.ExtSvc += [ 'AuditorSvc', 'ToolSvc' ]
  # Trigger svc! Without these there'll be lots of bugs...
  for svc in ('evtSvc', 'partSvc', 'histSvc' ):
    getattr(gaudi, svc, None)()

  return gaudi.algorithm(seq_name, createIf=False)
