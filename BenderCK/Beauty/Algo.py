#!/usr/bin/env python

from . import *

# from Bender.Main import *
# from Bender.MainMC import *

# from ..Functions import in_acceptance, iBotCopyId
# from ..Decorators import lazy_algo_property, count
# from ..BaseAlgo import AlgoMC  # Patched algo. Import me last....

# from MyPythonLib import log

class BaseBeautyAlgoMC(AlgoMC):

  @lazy_algo_property
  def list_bquark(self):
    return list(set(MyBeauty(p) for p in self.mcselect('bquark', MCABSID==5)))

  @lazy_algo_property
  def list_bbbar(self):
    """Return a ..."""
    l = self.mcselect('dibquark', (MCNINTREE(MCABSID==5)>=2) & (MC3Q==0) )     # May miss the level
    l = [ p for p in l if len([ c for c in p.children() if MCABSID(c)==5])>=2]  # Check fist level
    # log.debug('Pending new bbbar for conversion:\n%r'%l)
    # return [ MyDitau(p) for p in l ]
    return list(l)

#==============================================================================

class FilterReconstructibleBjetAlgoMC(AlgoMC):
  """
  Filter event with at least one b-jet which is LHCb-Reconstructible
  
  - Seek for b-quark MC-particle, strictly from `id_mother` if given in init.
  - Those b-seeds must be in acceptance.
  - Those b-seeds matched with one of the StdJets (via MCMATCH functor)
  - Those B-seed must be in the cone! (DR(jet)(seed) < 0.5)
  - There can be multiple match, but one match is enough (per b).
  """

  def __init__(self, name, id_mother=None):
    super(FilterReconstructibleBjetAlgoMC, self).__init__(name)
    self.id_mother = id_mother
    log.debug('id_mother: %r'%id_mother)

  @lazy_algo_property
  def list_B_seeds(self):
    """
    Return a (lazy) list of MC B-hadrons to be used as seeds.

    - If id_mother given, this is requirement for mother of B-hadron
    - May or maynot have b-quark intermediate (in some cases). Ask using `HADRON` functor
    - This must be first one in hadronic chain
    """

    l = list(self.mcselect('B', BEAUTY & HADRON))
    ## Filter mother if requested
    if self.id_mother:
      l = [ B for B in l if any( MCID(p)==self.id_mother for p in B.ancestors()) ]
    ## Remove chain duplicate, mutable loop using copy of l  ---> Don't, interfere with jet children
    # for B in list(l):
      # if any( p in l for p in B.ancestors()):
        # log.debug('Remove duplicate: %r'%B)
        # l.remove(B)
    return l
  
  @count 
  def pass_B_seeds_inacc(self):
    """Required at least one B-seed inacc"""
    return any(in_acceptance(B) for B in self.list_B_seeds)

  @count
  def have_B_jet_counterpart(self):
    # Check against jet requirements
    list_jet = list( self.get('Phys/StdJets/Particles') )
    for B in self.list_B_seeds:
      if in_acceptance(B):
        truth_func = MCTRUTH( B , self.mcTruth() )
        for jet in list_jet:
          val1 = any( truth_func( child ) for child in jet.children() )
          # val1 = truth_func(jet)
          val2 = (DR2( B.momentum() )( jet )**0.5) < 0.5
          if val1 and val2:
            log.debug('Matched! %r --> %r'%(B.momentum(), jet.momentum()))
            log.verbose(B)
            log.verbose(jet)
            return True
    return False

  #----------------------------------------------------------------------------

  def analyse(self):
    self.setFilterPassed(False)

    # Check acceptance
    if not self.pass_B_seeds_inacc():
      return SUCCESS

    # Check jets requirement
    if not self.have_B_jet_counterpart():
      return SUCCESS

    # All pass!
    self.setFilterPassed(True)
    return SUCCESS
