#!/usr/bin/env python

__all__ = ('MyBeauty', )

from . import *

report_verbose = report(level=log.VERBOSE, hide_input=True)

#==============================================================================

class MyBeauty(LHCb.MCParticle):
  """
  Helper class which facilitate the study of Beauty quark/hadron/jet.
  """

  def __init__(self, mcbquark):
    b = iBotCopyId(mcbquark)
    super(MyBeauty, self).__init__(b)
    ## BUG: Index was lost!
    self._index = b.index()
    self._validate()  # Autocheck

  def __hash__(self):
    """To uniquize the same item being recoiled. Rely on iBotCopyId"""
    return self._index
    # return self.index()

  def __eq__(self,other):
    """Need together with __hash__"""
    return hash(self)==hash(other)

  def index(self):
    raise ValueError("Don't use me. It's buggy")

  def _validate(self):
    ## I need you to be a b-quark (for full info)
    assert MCABSID(self)==5


#==============================================================================
