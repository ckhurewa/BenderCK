
import atexit
import functools
from PythonCK.decorators import optional_arg_decorator
from . import logger, SUCCESS

__all__ = (
  #
  'pass_success',
  'real_passthrough_success',
  #
  'count', 
  'static_algo_property',
  'lazy_algo_property',
  'lazy_algo_method',
)


#==============================================================================

def pass_success(analyse):
  """
  Useful functor for lazy people who can't bother write `self.setFilterPassed`
  and `return SUCCESS` all the time...

  Usage:
      @pass_success
      def analyse(self):
        # do something useful, probably read-only algo.
  """

  @functools.wraps(analyse)
  def wrap(self):
    try:
      analyse(self)
      self.setFilterPassed(True)
      return SUCCESS
    except Exception, e:
      logger.exception(e)
      ## Shutup, and exit
      print '='*40 + ' ABORTED by BenderCK ' + '='*40
      import os
      os._exit(-1)  # Abrupt exit
  return wrap

#==============================================================================

def real_passthrough_success(analyse):
  """
  Attach this decorator to skip analyse() entirely if source data is not MC.
  - Useful for those pure-AlgoMC monitors.
  - Included pass_success decorator at outer layer.
  """
  @functools.wraps(analyse)
  def wrap(self):
    if self.is_MC:
      analyse(self)
  return pass_success(wrap)

#==============================================================================

class base_cache_decorator(object):
  __slots__ = ('func', 'key', 'cache', 'count_hit', 'count_total')

  def __init__(self, func):
    self.func         = func
    self.key          = None
    self.count_hit    = 0
    self.count_total  = 0
    atexit.register(self.at_exit)

  def at_exit(self):
    # # Clear the cache # Too late for gc anyway
    # del self.func 
    # del self.key 
    # del self.cache 
    # Report stats
    if self.count_total == 0:
      rate = '---'
    else:
      val  = 1. * self.count_hit / self.count_total
      rate = '{:6}/{:6} = {:.2%}'.format(self.count_hit, self.count_total, val)
    msg  = 'Hit rate: {:24}: {:30} | {}'
    logger.debug(msg.format(self.__class__.__name__, self.func.__name__, rate))

#--------------------------------------------------------------

class static_algo_property(base_cache_decorator):
  """
  For property which is not depend on EvtNumber & RunNumber, but should be 
  cached and unchanged for single Algo instance. Perfect for tools.

  Usage:

  @static_algo_property
  def MCrec(self):
    return self.tool('IMCReconstructible', 'MCReconstructible')

  def analyse(self):
    p = Particle()
    print self.MCrec.inAcceptance(p)
    return SUCCESS
  """

  __slots__ = ()

  def __get__(self, obj, cls):
    try:
      if obj is None:
        return self
      if self.key:
        val = self.cache
        self.count_hit += 1
      else:
        val = self.cache = self.func(obj)
        self.key = True
      self.count_total += 1
      return val
    except Exception, e:
      logger.exception(e)
      raise e

#--------------------------------------------------------------

class lazy_algo_property(base_cache_decorator):
  __slots__ = ()

  def __get__(self, algo, cls):
    if algo is None:
      return self
    try:
      runevt = algo.runevt
      if runevt == self.key:
        val = self.cache 
        self.count_hit += 1
      else:
        val = self.cache = self.func(algo)
        self.key = runevt
      self.count_total += 1
      return val
    except Exception, e:
      logger.exception(e)
      raise e

#--------------------------------------------------------------

class lazy_algo_method(base_cache_decorator):
  """
  Another layer: Allow variety of args inside same event, but they're invalid
  upon new event.
  """
  __slots__ = ('algo',)

  def __get__(self, algo, cls):
    self.algo = algo
    return self

  def __call__(self, *args): # Simple, no **kwargs
    try:
      algo    = self.algo
      runevt  = algo.runevt
      if runevt != self.key: # New event, please clear cache
        self.cache = dict()
        self.key   = runevt
      if (runevt == self.key) and (args in self.cache):
        val = self.cache[args]
        self.count_hit += 1
      else:
        val = self.cache[args] = self.func(algo, *args)
      self.count_total += 1
      return val
    except Exception, e:
      logger.exception(e)
      raise e

#==============================================================================

#http://stackoverflow.com/questions/7492068/python-class-decorator-arguments

@optional_arg_decorator
class count(object):
  """
  Decorator for method INSIDE Bender. Process the result of method into counter.

  ## Support return types
  - boolean   : Increment count directly by one. 
  - int/float : Collect a-la histogram.
  - list      : Collect len(list)
  - string    : Incremental counter (suffix to func name).
                Note, by this scheme, it's not straight forward to do fraction str count.
  - None      : This is completely ignored.

  ## Sharding 
  In many cases, the counting is interested in its secondary classification 
  ( Such as when counting particle in acceptance, to group them by species ).
  The concept of `sharding` helps such classification by split the counter 
  into sub-counters if the *sharding flag* is given. The counting method for 
  each type listed above remain the same.

  Inner sharding (inside a decorated function):

      @count
      def mymethod(self, val):
          self.mymethod.sharding = 'text'  # Not on self, too obscure
          return val

  Outer sharding (outside a decorated function, inside an Algo/AlgoMC)

      def analyse(self):
          self.mymethod.sharding = 'text'
          self.mymethod(somevalue)
          return SUCCESS

  Args:
    (float) valmin: If provide, will do additional boolean-counter of `val > min`
    (float) valmax: If provide, will do additional boolean-counter of `val < max`
    (bool)  ntuple: If True, provide the ntuple of this variable as well.
  """

  __slots__ = ( 
    '_algo',
    '_extra',
    '_func',
    '_ntuple',
    '_valmin',
    '_valmax',
    'sharding',
  )

  def __init__(self, func, valmin=None, valmax=None, ntuple=False):
    self._func    = func
    self._valmin  = valmin
    self._valmax  = valmax
    self._ntuple  = ntuple
    self._extra   = logger.extra(func)

  # This is needed for class-based decorator acting on instance-method.
  # https://christiankaula.com/python-decorate-method-gets-class-instance.html
  def __get__(self, instance, owner):
    self._algo = instance
    return self

  def __call__(self, *args, **kwargs):
    ## Evaluate the function
    algo      = self._algo
    result    = self._func(algo, *args, **kwargs)
    basename  = self._func.__name__
    ## Ignore None 
    if result is None:
      return 
    ## Reject incompat type
    if not isinstance(result, ( bool, int, long, float, list, basestring )):
      logger.warning("Unknown counting strategy in %r: %r" % (basename , type(result)), extra=self._extra)
      return result
    ## Prepare key, then inflate list of entries to fill
    rawkey   = basename
    sharding = getattr(self, 'sharding', None)
    if sharding:
      rawkey += '/'+sharding
        # delattr(instance, 'sharding')
    ## Finally, fill into tuple
    for key,val in self._parse_atomic(rawkey, result):
      self._fill(algo, key, val)
    return result 


  def _parse_atomic(self, rawkey, result):
    """Handle to non-composite result type, yield the queue."""
    if rawkey is None:
      raise StopIteration
    elif isinstance(result, bool):
      yield rawkey, result
    elif isinstance(result, (int,long,float)):
      yield rawkey, result
      if self._valmin is not None:
        yield '%s: min=%r'%(rawkey,self._valmin), (result >= self._valmin)
      if self._valmax is not None:
        yield '%s: max=%r'%(rawkey,self._valmax), (result <= self._valmax)
    elif isinstance(result, list):
      yield rawkey, len(result)
    elif isinstance(result, basestring):      
      yield (rawkey+'/'+result), 1  # Simple increment
    else:
      raise ValueError('Unknown atomic type should have been caught: %r'%type(result))


  def _fill(self, algo, key, val):
    """
    Internal fill method. Request the counter (key) from algo and put with val.
    """
    logger.verbose('Fill: key=({}), val=({})'.format(key,val), extra=self._extra)
    counter   = algo.Counter(key)
    counter  += val 
    if self._ntuple and ':' not in key:  # Exclude the hist of min/max bool.
      key = key.replace('/','__')  # Better compat for ROOT
      tup = algo.nTuple(key)
      tup.column('value', val)  # Using tuple of 1-column only!
      tup.write()

#==============================================================================
