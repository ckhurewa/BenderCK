
import math
from . import (
  ROOT, LHCb, logger, Gaudi,
  MCID, MCE, MCPX, MCPY, MCPZ, MCETA, MCDPHI, MCDETA,
  E, ETA, PX, PY, PZ, DR2, IP, IPCHI2, VSAME,
)

#==============================================================================

def vsimilar(v1,v2, threshold=0.05):
  # Return true if two scalar are similar within certain threshold
  return 2*abs(v1-v2)/(abs(v1)+abs(v2)) < threshold

def psimilar(p1, p2, threshold_norm=0.20, threshold_angle=0.05):
  """
  Unorthodox method, return True if the 4-vector between MCP,P seems similar
  - Strict 3-vector direction (eta,phi), error < 5%
  - Loose  4-vector norm, error < 20%

  Note: Use the method instead of functor to be able to support comparison
  between p/mcp interchangeably.
  """
  # ## ID
  # if p1.particleID().pid() != p2.particleID().pid():
  #   return False 
  mmt1 = p1.momentum()
  mmt2 = p2.momentum()
  ## 3-direction (eta-phi)
  if not vsimilar(mmt1.eta(), mmt2.eta(), threshold_angle):
    return False
  if not vsimilar(mmt1.phi(), mmt2.phi(), threshold_angle):
    return False
  ## Norm (ignore Lorentz metric)
  norm1 = (mmt1.P()**2 + mmt1.E()**2)**0.5
  norm2 = (mmt2.P()**2 + mmt2.E()**2)**0.5
  if not vsimilar(norm1, norm2, threshold_norm):
    return False
  ## Finally
  return True

def MCSAME(p1, p2):
  """Return True if two LHCb.MCParticle is equivalent, more flexible."""
  if p1 is None or p2 is None:
    return False
  if p1==p2:
    return True
  return p1.momentum()==p2.momentum() and MCID(p1)==MCID(p2)

# def MCIN( mcp, l ):
#   """Return True if mcp is IN l, check via MCSAME function."""
#   return any( MCSAME(mcp,x) for x in l )

#==============================================================================

# PYTHIA8-inspired functions

def iBotCopyId(p):
  """Like Pythia's"""
  l = [c for c in p.children() if MCID(c)==MCID(p)]
  if len(l)>1:
    logger.warning(p)
    logger.warning(l)
    logger.exception('Many same child?? Return end-of-chain here.')
    return p
  return p if not l else iBotCopyId(l[0])

def iTopCopyId(p):
  l = [x for x in p.parents() if MCID(x)==MCID(p)]
  if len(l)>1:
    logger.warning(p)
    logger.warning(l)
    logger.exception('Many same parent?? Return end-of-chain here.')
    return p
  return p if not l else iTopCopyId(l[0])

#==============================================================================

# def MCVIP(mcp):
#   """Vectorial Impact Parameter"""
#   #https://groups.cern.ch/group/lhcb-davinci/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2flhcb-davinci%2fLists%2fArchive%2fImpact%20Parameter%20calculation%20for%20MC%20particles&FolderCTID=0x01200200B795ECDBD8943F4E90E5F02A017196C9
#   Line  = Gaudi.Math.Line("Gaudi::XYZPoint,Gaudi::XYZVector")
#   pv    = mcp.primaryVertex()
#   line  = Line( mcp.originVertex().position() , mcp.momentum().Vect() )
#   point = pv.position()
#   return Gaudi.Math.closestPointParam( point, line )

#   pv  = mcp.primaryVertex()
#   a   = mcp.originVertex().position()
#   p   = pv.position()
#   p   = Gaudi.XYZVector(p.x(), p.y(), p.z()) # Change to displacement vector. WTF!
#   mmt = mcp.momentum().Vect()
#   n   = mmt.Unit() # Return unit vector 
#   return (a-p)-((a-p).Dot(n))*n

def MCVIP(mcp):
  # closestPoint (const aPoint &point, const aLine &line)
  # Return aPoint on aLine which is closest to an aPoint.
  Line  = Gaudi.Math.Line("Gaudi::XYZPoint,Gaudi::XYZVector")
  line  = Line( mcp.originVertex().position() , mcp.momentum().Vect() )
  pv    = mcp.primaryVertex().position()
  point = Gaudi.Math.closestPoint( pv, line )
  return point - pv

def MCIP(mcp):
  """Impact parameter"""
  return MCVIP(mcp).R()
  # Line  = Gaudi.Math.Line("Gaudi::XYZPoint,Gaudi::XYZVector")
  # pv    = mcp.primaryVertex()
  # line  = Line( mcp.originVertex().position() , mcp.momentum().Vect() )
  # point = pv.position()
  # return Gaudi.Math.impactParameter( point, line )

def MCSIP(mcp):
  r = MCVIP(mcp)
  p = mcp.momentum().Vect()
  # Cross product is not working, workaround manually
  z = r.X()*p.Y() - r.Y()*p.X()
  sign = +1 if (z > 0) else -1
  return sign * r.R()


# _TrajPoca = None
# def VIP(p, v):
#   """
#   Vectorial Impact Parameter, as a func of ( Particle, Vertex ).
#   """
#   ## https://groups.cern.ch/group/lhcb-davinci/Lists/Archive/DispForm.aspx?ID=7524
#   try:
#     global _TrajPoca
#     if _TrajPoca is None:
#       _TrajPoca = appMgr().toolSvc().create('TrajPoca', interface=cpp.ITrajPoca)
#     dist    = LHCbMath.XYZVector()
#     traj    = LHCb.TrackTraj.TrackTraj( p.proto().track() )
#     mu      = ROOT.Double( 0.1 )
#     point   = v.position()
#     prec    = ROOT.Double( 5E-4 )
#     success = _TrajPoca.minimize( traj, mu, point, dist, prec )
#     if not success:
#       logger.error('Failed trajpoca.minimize')
#       raise ValueError
#     return dist
#   except Exception, e: # Possibly missing proto,
#     logger.exception(e)
#     logger.warning(p)
#     logger.warning(v)
#     return LHCbMath.XYZVector()

def MIP_vertices( impparbase, vertices, reco, default_value = -1. ):
  l = [ IP( v, impparbase )( reco ) for v in vertices ]
  return default_value if not l else min(l)


def MIP_P2PV( impparbase, table, reco, default_value = -1. ):
  """
  Calculate MIP given P2PV table.
  Access table.relations dircetly to handle the multi-candidate per event refitting.
  impparbase = algo.geo()
  """
  if not table:
    return default_value
  vertices = ( entry.to() for entry in table.relations(reco) )
  return MIP_vertices( impparbase, vertices, reco, default_value )

def SIP( dcalc, p, v ):
  """
  Signed Impact Parameter.
  Alternative approach with distance calculator
  """
  ipvec = Gaudi.XYZVector()
  mmt   = p.momentum().Vect()
  mmt   = Gaudi.XYZVector( mmt.X(), mmt.Y(), mmt.Z() )
  dcalc.distance( p, v, ipvec )
  z     = ipvec.X()*mmt.Y() - ipvec.Y()*mmt.X()
  return ipvec.R() * (1. if (z>0) else -1.)

# def SIP(p, v):
#   """
#   Signed Impact Parameter.
#   """
#   vip = VIP(p,v)
#   mmt = p.momentum()
#   z   = vip.X()*mmt.Y() - vip.Y()*mmt.X()
#   r   = vip.R()
#   return -1E6 if r==0 else r if z>=0 else -1*r

# def SMIP( dcalc, p, vertices ):
#   """
#   Signed Minimized Impact Parameter.
#   """
#   res = sorted([ SIP(dcalc,p,v) for v in vertices ], key=lambda x: abs(x))
#   return res[0] if res else -1E6

def IPSIG( impparbase, particles, vertex ):
  """Calculate multi-particle ImpactParamSignificance for given single vertex."""
  sum_sip   = 0.
  sum_err2  = 0.
  Ipchi2    = IPCHI2( vertex, impparbase )
  for p in particles:
    sip   = SIP(impparbase.tool(), p, vertex )
    chi2  = Ipchi2(p)
    if chi2 < 0:  # 
      return -1.  # Immeidately abort!
      # raise ValueError('CheckME!')
      # continue  # Skip this particle 
    sum_sip  += sip 
    sum_err2 += sip**2/chi2 
  return abs(sum_sip)/(sum_err2**0.5) if sum_err2>0 else -1.

def MIPSIG_P2PV( impparbase, table, mother ):
  """Notice different negative number is used as error-val"""
  if not table: 
    return -2. 
  res = [ ]
  for relat in table.relations(mother):
    res.append(IPSIG( impparbase, mother.children(), relat.to() ))
  return min(res) if res else -3.

def MIPSIG( impparbase, particles, vertices ):
  """
  Given list of vertices, return smallest IPS made over single vertex.

  - IPS (Impact Parameter Significance) is defined as abs(sum(SMIP)) / ERR 
  - SMIP is signed impact parameter.
  - ERR is associated error with the above IP, added quadratically.

  Note: ERR**2 = VAL**2 / CHI2

  Note: impparbase is ImpParBase, subclass of ImpactParamTool, which is 
        a wrapper around IDistanceCalculator. 
        This impparbase is available from Algo.geo().
  """
  if not vertices or len(vertices)==0:
    return -1.
  return min( IPSIG(impparbase,particles,v) for v in vertices )


  # result = []
  # for v in vertices:
  #   valid     = True
  #   sum_sip   = 0
  #   sum_err2  = 0
  #   Ipchi2    = IPCHI2(v,impparbase)
  #   for p in particles:
  #     sip   = SIP(impparbase.tool(), p, v)    # raise -1E6
  #     chi2  = Ipchi2(p)
  #     valid &= (chi2>0)
  #     if not valid:
  #       return -1.
  #     sum_sip   += sip
  #     sum_err2  += sip**2/chi2
  #   result.append( abs(sum_sip)/sum_err2**0.5 )
  # return min(result) if result else -1. 



# class __MCSIP(object):
#   """Signed Impact Parameter"""
#   def __str__(self):
#     return "MCSIP"
#   def __call__(self, mcp):
#     vip   = MCVIP(mcp)
#     sign  = +1 if vip.z() > 0 else -1
#     return vip.R() * sign 

# MCSIP = __MCSIP()


#==============================================================================

def refits( pvrefitter, particles, vertices ):
  """
  Return list of new vertices, where those are refitted, excluding tracks
  from input particles.
  """
  res = LHCb.VertexBase.ConstVector()
  for v in vertices:
    v2 = v.clone()
    for p in particles:
      pvrefitter.remove(p,v2)
    res.push_back(v2)
  return res


#==============================================================================

# def VSAME( v1, maxdist=-1., maxchi2=-1., minfrac=-1. ):

def VSIM( v1, minfrac=None ):
  """
  Crux of the AALLSAMEBPV which is poorly implemented (also with mistake).
  Return True if second vertex is similar enough to the first one.

  Usage:
  >> VSIM( v1, minfrac=0.8 )( v2 )
  True 

  """
  ## Return dummy for null-vertex
  if not v1:
    return lambda v2: False

  ## Prepare ingredients for v1
  Vsame      = VSAME(v1)
  check_frac = minfrac is not None
  if check_frac:
    if minfrac < 0. or minfrac > 1.:
      raise ValueError('Invalid argument: minfrac not in [0,1]')
    tracks1 = { tr.key() for tr in v1.tracks()}
    ntr1    = len(tracks1)
  def _VSIM( v2 ):
    if v1==v2:  # pointer equality
      return True 
    if Vsame(v2):  # Alternative LoKi equality
      return True
    if check_frac:
      tracks2 = { tr.key() for tr in v2.tracks()}
      ntr2    = len(tracks2)
      ntr0    = len(tracks1 & tracks2)  # intersect
      
      # print v1.position(), v2.position(), (v1.position()-v2.position()).Mag2()**0.5, (ntr0 >= minfrac*ntr1) or (ntr0 >= minfrac*ntr2)

      if (ntr0 >= minfrac*ntr1) or (ntr0 >= minfrac*ntr2):
        return True 
    return False 
  return _VSIM


#==============================================================================

def MCDR(mcp1, mcp2):
  """Because the original is somewhat missing..."""
  return (MCDETA(mcp1)(mcp2)**2+MCDPHI(mcp1)(mcp2)**2)**0.5


def MCDR_trio(p1, p2, p3):
  """return ascending-sorted MCDR between 3 combinations."""
  return sorted([ MCDR(p1,p2), MCDR(p2,p3), MCDR(p3,p1) ])
  # import itertools
  # return sorted( DR(p1)(p2) for p1,p2 in itertools.combinations(list_p, 2) )


def DR_trio(p1, p2, p3):
  """return sorted DR ascending between 3 combinations."""
  return sorted([ DR2(p1)(p2)**0.5, DR2(p2)(p3)**0.5, DR2(p3)(p1)**0.5 ])


#==============================================================================

def MM_reweight( vec41, mass1, vec42, mass2 ):
  """
  Recalc the invariant mass of vec41+vec42, such that for each vec4 it got 
  reweight into given weights.
  Used in calc of ditau invar mass correction caused by tau_h3 children.

  Args: 
    vec4: instance of ROOT.Math.LorentzVector("ROOT::Math::PxPyPzE4D<double>")
  """
  scale1 = mass1 / vec41.M()
  scale2 = mass2 / vec42.M()
  return ((vec41*scale1)+(vec42*scale2)).M()



#==============================================================================

def in_acceptance(p, etamin=2.0, etamax=4.5):
  """Return True if ETA of particle is in given acceptance"""
  if isinstance(p, LHCb.Particle):
    return ETA(p) > etamin and ETA(p) < etamax
  else:
    return MCETA(p) > etamin and MCETA(p) < etamax


def variance(p1, p2, relative=False):
  """Return variance of the difference between 2 four-vectors (from 2 particles)"""
  v     = p1.momentum()-p2.momentum()
  w     = 0.5*(p1.momentum()+p2.momentum())
  varv  = v.P2()+(v.E())**2
  denom = w.P2()+(w.E())**2
  return varv/denom if relative else varv

# Convert to ROOT
def TLzV(p):
  if isinstance(p, LHCb.Particle):
    return ROOT.TLorentzVector(PX(p), PY(p), PZ(p), E(p))
  elif isinstance(p, LHCb.MCParticle):
    return ROOT.TLorentzVector(MCPX(p), MCPY(p), MCPZ(p), MCE(p))
  return None


def VEC4RERR(p1, p2):
  """Return relative error between 2 LorentzVector"""
  return variance(p1,p2,relative=True)**0.5


def phistar( eta1, eta2, phi1, phi2 ):
  """Use in W/Z paper."""
  deta = eta1-eta2 
  dphi = ROOT.TVector2.Phi_mpi_pi( phi1-phi2 )
  return math.tan((math.pi - abs(dphi))/2) / math.cosh(deta/2)
