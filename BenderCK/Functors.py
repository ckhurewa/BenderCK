"""
My LoKi-Bender Functors

Unlike functions, this is expected to be working inside LoKi::Hybrid::Engine.
"""

from . import *

## Expose more import, in case this module is loaded inside DaVinci's preambulo

# clsAux  = LoKi.AuxFunBase
# clsFunc = LoKi.Functor('LHCb::Particle const*,double')
# class _TESTFUNC(object):
#   # def __init__(self):
#   #   super(_TESTFUNC, self).__init__()
#   def clone(self):
#   #   return self.__class__.__init__()
#     pass
#   def __call__(self, p):
#     return 42
#   def eval(self, x):
#     return self(x)
#   def evaluate(self, x):
#     return self(x)
#   def __del__(self):
#     pass
#   def __repr__(self):
#     return '42'
# TESTFUNC = _TESTFUNC()


# class _TESTFUNC(object):
#   """Used as momentum relative-error of a particle. Return 0 if P==0."""
#   def __str__(self):
#     return "TESTFUNC"
#   def __call__(self, obj):
#     return 42
# TESTFUNC = _TESTFUNC()


# class _DRTRIO(clsFunc):
#   """Using Functions.DR_trio as base, make a functor version taking parents."""
#   def __init__(self, name, fmod):
#     # fmod is the final func to apply on resultant len-3 array.
#     # expect: min, max, 
#     self._name = name
#     self._fmod = fmod
#   def __str__(self):
#     return self._name
#   def __repr__(self):
#     return self._name
#   def clone(self):
#     print 'CLONEEEEEEEEEEEEEEEEEEE'
#     return self.__class__.__init__(self._name, self._fmod)
#   def __call__(self, p):
#     drtrio = Functions.DR_trio(*p.children()) # It'll compain if len!=3
#     return self._fmod(drtrio)


# DRTRIOMIN = _DRTRIO("DRTRIOMIN", min)
# # DRTRIOMID = ...
# DRTRIOMAX = _DRTRIO("DRTRIOMAX", max)

# class DRTRIOMIN:
#   """Using Functions.DR_trio as base, make a functor version taking parents."""
#   # def __init__(self, name, fmod):
#   #   # fmod is the final func to apply on resultant len-3 array.
#   #   # expect: min, max, 
#   #   self._name = name
#   #   self._fmod = fmod
#   # def __str__(self):
#   #   return self._name
#   # def __repr__(self):
#   #   return self._name
#   def clone(self):
#     print 'CLONEEEEEEEEEEEEEEEEEEE'
#     pass
#   def __call__(self, p):
#     drtrio = Functions.DR_trio(*p.children()) # It'll compain if len!=3
#     return min(drtrio)


# def DRTRIOMIN(p):
#   drtrio = Functions.DR_trio(*p.children()) # It'll compain if len!=3
#   return min(drtrio)


# class __PRERR(object):
#   """Used as momentum relative-error of a particle. Return 0 if P==0."""
#   def __str__(self):
#     return "PRERR"
#   def __call__(self, obj):
#     return float(-1) if (P(obj)==0) else (PERR2(obj)**0.5 / P(obj))

# PRERR = __PRERR()


#===============================================================================
# Use in Tau.Framework

MCISTAU    = MCABSID==15
MCISTAUE   = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & (MCABSID==11), True)==1)
MCISTAUMU  = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & (MCABSID==13), True)==1)
MCISTAUH1  = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON, True)==1)
MCISTAUH3  = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON, True)==3)
MCTAUITYPE = switch(~MCISTAU, -1, switch( MCISTAUE, 0, switch( MCISTAUH1, 1, switch( MCISTAUH3, 2, switch( MCISTAUMU, 3, 4)))))


# #==============================================================================
# # Isolation cone series
# #==============================================================================

# ### Isolation cone
# NCone01A  = SUMCONE(0.1**2, ONE, 'Phys/PFParticles')
# NCone01C  = SUMCONE(0.1**2, ONE, 'Phys/PFParticles', Q!=0)
# NCone04A  = SUMCONE(0.4**2, ONE, 'Phys/PFParticles')
# NCone04C  = SUMCONE(0.4**2, ONE, 'Phys/PFParticles', Q!=0)
# NCone05A  = SUMCONE(0.5**2, ONE, 'Phys/PFParticles')
# NCone05C  = SUMCONE(0.5**2, ONE, 'Phys/PFParticles', Q!=0)
# #
# # ECone02_TRBEST     = SUMCONE(0.2**2, E  , 'Rec/Track/Best')
# # ECone04_TRBEST     = SUMCONE(0.4**2, E  , 'Rec/Track/Best')
# # ECone05_TRBEST     = SUMCONE(0.5**2, E  , 'Rec/Track/Best')

# ECone02A = SUMCONE(0.2**2, E  , 'Phys/PFParticles')
# ECone04A = SUMCONE(0.4**2, E  , 'Phys/PFParticles')
# ECone05A = SUMCONE(0.5**2, E  , 'Phys/PFParticles')
# ECone07A = SUMCONE(0.7**2, E  , 'Phys/PFParticles')
# ECone02C = SUMCONE(0.2**2, E  , 'Phys/PFParticles', Q!=0)
# ECone04C = SUMCONE(0.4**2, E  , 'Phys/PFParticles', Q!=0)
# ECone05C = SUMCONE(0.5**2, E  , 'Phys/PFParticles', Q!=0)
# ECone07C = SUMCONE(0.7**2, E  , 'Phys/PFParticles', Q!=0)
# ECone02N = SUMCONE(0.2**2, E  , 'Phys/PFParticles', Q==0) # Neutral
# ECone05N = SUMCONE(0.5**2, E  , 'Phys/PFParticles', Q==0)

# # new 150429 (cleaning better tau_h1)
# ECone05_PFCHGNGHST = SUMCONE(0.5**2 , E  , 'Phys/PFParticles', (Q!=0)&PPCUT(PP_ProbNNghost>=0) )
# ECone005_PFEM      = SUMCONE(0.05**2, E  , 'Phys/PFParticles', (ABSID==11)|(ABSID==22) ) # Detect very close pi0->e/gamma

# # Derived ECone-Fraction
# EFrac05C      = E / ( E+ECone05C )
# EFrac05A      = E / ( E+ECone05A )
# EFrac07C      = E / ( E+ECone07C )
# EFrac07A      = E / ( E+ECone07A )
# ECone02PN     = E + ECone02N                  # self+neutral
# EFrac02PN05N  = ECone02PN / ( E + ECone05N )  # (self+neutral) / (self+neutral)
# EFrac02PN05A  = ECone02PN / ( E + ECone05A )  # (self+neutral) / (self+neutral+charged)

# # ## PT-vectorial cone
# # BAD_PX2Cone05_PFCHARGED = SUMCONE(0.5**2, PX**2 , 'Phys/PFParticles', Q!=0)
# # BAD_PY2Cone05_PFCHARGED = SUMCONE(0.5**2, PY**2 , 'Phys/PFParticles', Q!=0)
# # BAD_PTCone05C  = (BAD_PX2Cone05_PFCHARGED + BAD_PY2Cone05_PFCHARGED)**0.5
# # BAD_PTFrac05C  = PT / ( PT + BAD_PTCone05C )

# PXCone05_PFALL     = SUMCONE(0.5**2, PX, 'Phys/PFParticles')
# PYCone05_PFALL     = SUMCONE(0.5**2, PY, 'Phys/PFParticles')
# PXCone05_PFCHARGED = SUMCONE(0.5**2, PX, 'Phys/PFParticles', Q!=0)
# PYCone05_PFCHARGED = SUMCONE(0.5**2, PY, 'Phys/PFParticles', Q!=0)
# PXCone05_PFNEUTRAL = SUMCONE(0.5**2, PX, 'Phys/PFParticles', Q==0)
# PYCone05_PFNEUTRAL = SUMCONE(0.5**2, PY, 'Phys/PFParticles', Q==0)

# PTCone05A  = (PXCone05_PFALL**2     + PYCone05_PFALL**2    )**0.5
# PTCone05C  = (PXCone05_PFCHARGED**2 + PYCone05_PFCHARGED**2)**0.5
# PTCone05N  = (PXCone05_PFNEUTRAL**2 + PYCone05_PFNEUTRAL**2)**0.5

# PTFrac05A  = PT / ( PT + PTCone05A )
# PTFrac05C  = PT / ( PT + PTCone05C )
# PTFrac05N  = PT / ( PT + PTCone05N )


#==============================================================================
# TRIO Series
#
# Useful functors for tau_h3 in Tau Selection analysis
#
#==============================================================================

from LoKiCore.math import *

def _ChildrenDPHI(i1,i2):
  from LoKiPhys.decorators import CHILD,PHI  
  from LoKiCore.math import cos,acos
  DPHI = abs(CHILD(PHI,i1)-CHILD(PHI,i2))
  DPHI = acos(cos(DPHI))  # mod pi       
  return DPHI

def _ChildrenAPT(i1,i2):
  pt1 = CHILD(PT,i1)
  pt2 = CHILD(PT,i2)
  return abs(pt1-pt2)/(pt1+pt2)

def _ChildrenDR(i1, i2):                           
  from LoKiPhys.decorators import CHILD,ETA
  DETA = abs(CHILD(ETA,i1)-CHILD(ETA,i2))
  DPHI = _ChildrenDPHI(i1,i2)   
  return (DPHI**2 + DETA**2)**0.5


def _MakeTrio(f1, f2, f3):
  """Helper method to generate trio-functors for p with 3 children."""
  from LoKiCore.math import min as lkmin
  from LoKiCore.math import max as lkmax
  from LoKiPhys.decorators import NDAUGHTERS
  from LoKiCore.functions import switch
  fmin = lkmin(f1, f2, f3)
  fmax = lkmax(f1, f2, f3)
  fmid = (f1+f2+f3) - fmin - fmax
  # also with guard.
  trioguard = lambda f: switch(NDAUGHTERS==3, f, -1)
  return trioguard(fmin), trioguard(fmid), trioguard(fmax)

def _MakeTrioSimple(f):
  ## Derivative of above func, apply via CHILD automatically
  return _MakeTrio(CHILD(f,1), CHILD(f,2), CHILD(f,3))

def _MakeTrioCyclic(f):
  ## Derivative of above func
  return _MakeTrio( f(1,2), f(2,3), f(3,1) )


PTTRIOMIN      , PTTRIOMID      , PTTRIOMAX       = _MakeTrioSimple(PT)
TRPCHI2TRIOMIN , TRPCHI2TRIOMID , TRPCHI2TRIOMAX  = _MakeTrioSimple(TRPCHI2)
DRTRIOMIN      , DRTRIOMID      , DRTRIOMAX       = _MakeTrioCyclic(_ChildrenDR)
DOCATRIOMIN    , DOCATRIOMID    , DOCATRIOMAX     = _MakeTrioCyclic(DOCA)
DOCACHI2TRIOMIN, DOCACHI2TRIOMID, DOCACHI2TRIOMAX = _MakeTrioCyclic(DOCACHI2)

C12_DPHI    = _ChildrenDPHI(1,2)
C12_DR      = _ChildrenDR(1,2)
C12_PHISTAR = 1. / tan(C12_DPHI/2) / cosh((CHILD(ETA,1)-CHILD(ETA,2))/2)

#==============================================================================
# Misc
#==============================================================================

# # http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/davinci/releases/latest/doxygen/db/d2d/class_lo_ki_1_1_a_particles_1_1_all_same_best_p_v.html
# AALLSAMEBPV_F02   = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1,  -1, 0.2 ), 1, 0 ))
# AALLSAMEBPV_F05   = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1,  -1, 0.5 ), 1, 0 ))
# AALLSAMEBPV_F08   = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1,  -1, 0.8 ), 1, 0 ))
# AALLSAMEBPV_C100  = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1, 100,  -1 ), 1, 0 ))
# AALLSAMEBPV_C10   = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1,  10,  -1 ), 1, 0 ))
# AALLSAMEBPV_C1    = PFUNA(switch( LoKi.AParticles.AllSameBestPV.AllSameBestPV( -1,   1,  -1 ), 1, 0 ))

## Calo
HCALFrac = PPFUN(PP_CaloHcalE)/P
ECALFrac = PPFUN(PP_CaloEcalE)/P

## Track Uncertainties (squared), at LastMeasurement
# Used in IPS study, but depreciated later.
# diag = lambda i: TrCOV2( LHCb.State.LastMeasurement, i, i)
# TRUNCER2 = TRFUN(diag(0)+diag(1)+diag(2)+diag(3)+diag(4))

#=========================================
# FUNCDICT Suite
#
# TupleTool, my way.
#=========================================

# not too large, please. See TTree::SetAlias
FD_MCKINEMATICS = {
  'MCM'   : MCM,
  'MCP'   : MCP,
  'MCPT'  : MCPT,
  'MCETA' : MCETA,
  'MCPHI' : MCPHI,
}

FD_MCPID = {
  'MCID'      : MCID,
  'MCMOMID'   : MCMOTHER( MCID , 0 ),
  'MCGMOMID'  : MCMOTHER(MCMOTHER( MCID , 0 ), 0 ),
  'MCGGMOMID' : MCMOTHER(MCMOTHER(MCMOTHER( MCID , 0 ), 0 ), 0 ),
}

# FD_MCMOM2CHILDREN = {
#   'MCDPHI',
#   'MCAPT'
# }

FD_KINEMATIC = {
  'M'     : 'M',
  'P'     : 'P',
  'PT'    : 'PT',
  'PZ'    : 'PZ',
  'ETA'   : 'ETA',
  'PHI'   : 'PHI',
  'PERR2' : 'PERR2',
}

## Info on single track
FD_TRACK = {s:s for s in (
  'TRCHI2DOF',
  'TRCHI2',
  'TRPCHI2',
  'TRGHOSTPROB',
  'TRTYPE',
)}

FD_PID = {
  'ID'         : ID,
  'ISMUON'     : switch(ISMUON     , 1, 0),  
  'ISLOOSEMUON': switch(ISLOOSEMUON, 1, 0),
  'PROBNNghost': PROBNNghost,
}

FD_PROTOPID = {
  'PP_CaloEcalE'    : 'PPFUN(PP_CaloEcalE)',
  'PP_CaloHcalE'    : 'PPFUN(PP_CaloHcalE)',
  'PP_CaloPrsE'     : 'PPFUN(PP_CaloPrsE)',
  'PP_CaloSpdE'     : 'PPFUN(PP_CaloSpdE)',
  'PP_InAccMuon'    : 'PPFUN(PP_InAccMuon)',
  'PP_InAccEcal'    : 'PPFUN(PP_InAccEcal)',
  'PP_InAccHcal'    : 'PPFUN(PP_InAccHcal)',
  'HCALFrac'        : 'HCALFrac',
  'ECALFrac'        : 'ECALFrac',
}

## On a single vertex
FD_VERTEX = {
  'VCHI2'       : 'VFASPF(VCHI2)',
  'VCHI2PDOF'   : 'VFASPF(VCHI2PDOF)',
  'VPCHI2'      : 'VFASPF(VPCHI2)',
  # 'AALLSAMEBPV' : 'PFUNA(switch(AALLSAMEBPV, 1, 0))',
  'ADOCAMIN'    : 'switch(VFASPF(VCHI2)!=-1., PFUNA(ADOCAMIN("")), -1.)',
  'ADOCAMAX'    : 'switch(VFASPF(VCHI2)!=-1., PFUNA(ADOCAMAX("")), -1.)',
  'ADOCACHI2'   : 'switch(VFASPF(VCHI2)!=-1., PFUNA(ADOCACHI2("")), -1.)',
}

## REMARL: ALWAYS think about PV-refit when using these BPVSERIES
FD_BPVSERIES = {
  'BPVCORRM'   : 'BPVCORRM',
  'BPVDIRA'    : 'BPVDIRA',
  'BPVDLS'     : 'BPVDLS',
  'BPVIP'      : 'BPVIP()',
  'BPVIPCHI2'  : 'BPVIPCHI2()',
  'BPVLTIME'   : 'switch(VFASPF(VCHI2)!=-1., BPVLTIME(), -1.)',
  'BPVPROJDIST': 'switch(VFASPF(VCHI2)!=-1., LoKi.Particles.ProjectedDistanceWithBestPV(), -1.)',
  'BPVPROJDS'  : 'switch(VFASPF(VCHI2)!=-1., LoKi.Particles.ProjectedDistanceSignificanceWithBestPV(), -1.)',
  'BPVVD'      : 'BPVVD',
  'BPVVDCHI2'  : 'BPVVDCHI2',
  'BPVVDRHO'   : 'BPVVDRHO',
  'BPVVDSIGN'  : 'BPVVDSIGN',
  'BPVVDZ'     : 'BPVVDZ',
}

## Made especially for mom having 2 children
FD_MOM2CHILDREN = {
  # Delta-phi between 2 children
  'DPHI'    : str(C12_DPHI),
  'DR'      : str(C12_DR),
  # PT-Asymmetry between 2 children (diff/sum)
  'APT'     : str(_ChildrenAPT(1,2)),
  'DOCA'    : 'PFUNA(ADOCAMIN(""))',
  'DOCACHI2': 'PFUNA(ADOCACHI2(""))',
  'PHISTAR' : str(C12_PHISTAR),

}

## Especially for mother with 3 children (e.g., 3-prongs tau)
FD_MOM3CHILDREN = { s:s for s in (
  'DRTRIOMIN',
  'DRTRIOMID',
  'DRTRIOMAX',
  'PTTRIOMIN',
  'PTTRIOMID',
  'PTTRIOMAX',
)}


def AChildSummary(fd):
  """
  Given a funcdict, return a new funcdict applying the original's functors 
  onto children, interest only in their MIN/MAX/MEAN. So this will be useful 
  only for particle having children.

  - Mechanism based on PFUNA.
  - Cache me if you can.

  Usage:
  
      FD_children = AChildSummary(FD_BASIC)

  """
  result = { 'NDAUGHTERS': NDAUGHTERS }
  for key,func in fd.iteritems():
    # Separate support between real/string functor version
    if isinstance(func, basestring):
      result[ key+'_CMIN' ] = 'PFUNA(AMINCHILD(%s))'%func
      result[ key+'_CMAX' ] = 'PFUNA(AMAXCHILD(%s))'%func
      result[ key+'_CMEAN'] = 'PFUNA(ASUM(%s)/ANUM(ALL))'%func
    else:
      result[ key+'_CMIN' ] = PFUNA(AMINCHILD(func))
      result[ key+'_CMAX' ] = PFUNA(AMAXCHILD(func))
      result[ key+'_CMEAN'] = PFUNA(ASUM(func)/ANUM(ALL))
  return result




# ## To be use on TupleToolMCTruth
# MCISTAU       = MCABSID==15
# MCISTAUH1     = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON)==1, True)
# MCISTAUH3     = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON)==3, True)
# MCISTAUE      = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & (MCABSID==11))==1, True)
# MCISTAUMU     = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & (MCABSID==13))==1, True)
# MCMOMISTAUE   = LoKi.Cuts.MCSWITCH(MCMOTHCUT( MCISTAUE  , False ), 1, 0)
# MCMOMISTAUH1  = LoKi.Cuts.MCSWITCH(MCMOTHCUT( MCISTAUH1 , False ), 1, 0)
# MCMOMISTAUH3  = LoKi.Cuts.MCSWITCH(MCMOTHCUT( MCISTAUH3 , False ), 1, 0)
# MCMOMISTAUMU  = LoKi.Cuts.MCSWITCH(MCMOTHCUT( MCISTAUMU , False ), 1, 0)
