#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
from collections import defaultdict, Counter
from random import random

from .. import MyTau, MyDitau
from ... import (
  AlgoMC, switch, logger, Functions,
  E, PT, CHARGED, ID, PX, PY, TOS, Q, HASPROTOS, DR2, HADRON, ABSID,
  MCPT, MCPX, MCPY, MCALL, MCABSID, MCDR2, MCID,
)
from ...Decorators import lazy_algo_property, static_algo_property

#==============================================================================

def zip_combine_tauh3(p1, p2, p3, l1, l2, l3):
  """
  Uniquely used in tauh3 construction.
  """
  ## Sort by overall-least variance
  var     = Functions.variance
  keyfunc = lambda p123: var(p123[0],p1,True)+var(p123[1],p2,True)+var(p123[2],p3,True)
  ## Make a triplet, any null-list will cease the loop rightaway
  return sorted([ comb for comb in itertools.product(l1, l2, l3) if len({x.index() for x in comb})==3 ], key=keyfunc)

#==============================================================================

class BaseTauAlgoMC(AlgoMC):

  """
  BaseTauAlgoMC: Provide the basic Algo to study Tau in MC

  ### List of getter

  - list_tau_plus   : 
  - list_tau_minus  : 
  - list_ditau      :

  ### List of methods

  - tau_fully_inacc
  - ditau_fully_inacc
  - ditau_fully_reconstructible
  - ditau_fully_reconstructed

  ### Features
  > 

  """

  def __init__(self, *args, **kwargs):
    ## Auto-default PFParticles
    # kwargs['Inputs'] = list(kwargs.get('Inputs', [])) +[ 'Phys/PFParticles' ]
    super(BaseTauAlgoMC, self).__init__(*args, **kwargs)


  @lazy_algo_property
  def list_tau(self):
    if not self.is_MC:
      return []
    # return self.list_tau_minus + self.list_tau_plus
    return list(set( MyTau(tau) for tau in self.mcselect('tau', MCABSID==15)))
    # return list(MyTau(tau) for tau in self.mcselect('tau', MCABSID==15))


  @lazy_algo_property
  def list_ditau(self): 
    if not self.is_MC:
      return []   
    # Both ditau must shared same mother
    # decayOnly=True is buggy...
    # l = self.mcselect('ditau', (MCNINTREE(MCID=='tau-')>=1) & (MCNINTREE(MCID=='tau+')>=1) & (MC3Q==0) ) 
    # l = [ p for p in l if len([ c for c in p.children() if MCABSID(c)==15])==2]
    l = self.mcselect('ditau', 'X0 => tau- tau+')
    logger.debug('Pending new ditau for conversion:\n%r'%l)
    return [ MyDitau(p) for p in l ]

    # Don't use MCNINTREE to stay only 1 level (exclude grandparent)
    # func = MCCHILDCUT(MCABSID=='tau+',1) & MCCHILDCUT(MCABSID=='tau+',2)

  @property 
  def list_ditau_weighted(self):
    """
    Return list of MC ditau in the system, just like Algo.list_ditau
    but the result is weighted (prescale) by dtype BR ( larger prescale on larger 
    channel such as h1h1 ). This allows the filesize to be more evenly-distributed
    and more disk-friendly to the offline analysis.

    Not cached, already cached in list_ditau, so I can yield.
    """

    weights = {
      'e_e'  : 1.0,
      'e_h1' : 0.2,
      'e_h3' : 1.0,
      'e_mu' : 1.0,
      'h1_h1': 0.1,
      'h1_h3': 0.3,
      'h1_mu': 0.2,
      'h3_h3': 1.0,
      'h3_mu': 1.0,
      'mu_mu': 1.0,
    }

    for ditau in self.list_ditau:
      prescale = weights.get( ditau.type, 1.0 )
      if random() < prescale:
        yield ditau

  @lazy_algo_property
  def tau_event_summary(self):
    """
    Provide an event-summary with info related to Tau. Design like EventTupleTool.
    Note: No is_MC guard here in order to let data mixing between Mc-real. 
    Guarded at the level of self.list_tau instead.

    return list of (key,val) so that it can be used in enhancedTuple.fill_gen
    """
    res = {}
    ## 1. Count of all tau by type (for purity study)
    res.update({'tau_type_'+s:0 for s in MyTau.TYPES })
    res.update(Counter('tau_type_'+tau.type for tau in self.list_tau))
    ## 2. Count all ditau types (in case of more than 1 ditau in single event)
    res.update({'ditau_type_'+s:0 for s in MyDitau.TYPES })
    res.update(Counter('ditau_type_'+ditau.type for ditau in self.list_ditau))
    ## Finally
    return res.items()

  #----------------#
  # SMART MATCHERS #
  #----------------#

  def mcMatch_tau(self, taucand):
    """
    Given a reco particle, return MCParticle if this reco matches with any of 
    the true MC tau (product) in this system., return None otherwise.

    Note, the otherway round (MC -> reco) is more tricky and less preferable 
    because it also requires the reco-container to be specified.

    Note: For tau_h3, return instance of MyTau (unlike e/mu/h1 where child 
      of tau is returned).

    Note: Since this is highly customized to look for mcMatch against tau only,
    any other invalid input ( not electron, muon, pion, kaon, tau ) will
    immediately return None

    Note-to-self: Can HASPROTOS functor be helpful?
    """

    ## Abort of this's not MC sample
    if not self.is_MC:
      return None
    ## Abort if type is not supported.
    absid = ABSID(taucand)
    if absid not in ( 11, 13, 15, 211, 321 ): # Not support type
      return None

    ## Candidate tau_h3
    if absid==15 and taucand.nChildren()==3:
      return self._mcMatch_tauh3( taucand )

    ## Simple single-child case
    mcpbest = self.find_mcp_best(taucand)
    if absid == 13:
      return mcpbest if mcpbest in self._list_tau_mu_true_mcpcand else None
    if absid == 11:
      return mcpbest if mcpbest in self._list_tau_e_true_mcpcand else None
    if absid in (211,321):
      return mcpbest if mcpbest in self._list_tau_h1_true_mcpcand else None

    raise NotImplementedError('absid==%r'%absid)

  def _mcMatch_tauh3( self, tauh3 ):
    """
    Return MyTau if the given reconstructed particle matched with MC tauh3.
    Return None if matching fails.
    """
    reco1, reco2, reco3 = list(tauh3.children())
    l1 = self.find_mcp_all( reco1 )
    l2 = self.find_mcp_all( reco2 )
    l3 = self.find_mcp_all( reco3 )
    logger.debug(l1)
    logger.debug(l2)
    logger.debug(l3)
    triplets = zip_combine_tauh3( reco1, reco2, reco3, l1, l2, l3 ) # made unique
    if not triplets: # Some MC is missing
      return None
    mc1,mc2,mc3 = triplets[0]
    logger.debug(mc1)
    logger.debug(mc2)
    logger.debug(mc3)
    mcmom1 = Functions.iTopCopyId(mc1).mother()
    mcmom2 = Functions.iTopCopyId(mc2).mother()
    mcmom3 = Functions.iTopCopyId(mc3).mother()
    logger.debug(mcmom1)
    logger.debug(mcmom2)
    logger.debug(mcmom3)
    if None in ( mcmom1, mcmom2, mcmom3 ): # Some mother missing
      return None
    if not (mcmom1==mcmom2==mcmom3):
      return None
    if MCABSID(mcmom1) != 15: # Who the hell are you?
      return None
    return MyTau( mcmom1 )

  def find_reco_best_tauh3(self, container, mcp, flag=None):
    """
    Like the standard find_reco_best, but made especially for tau_h3 (MyTau).

    Return trio [pi,pi,pi]. The key feature is to make sure that the 
    matched trio doesn't contain duplicate from matching, thus internally it 
    relies on `find_reco_all` instead of `find_reco_best`.

    If flag is given, write the user-addInfo at that adress to mark whether
    agressive search was used or not, to benchmark improvement.
    """
    assert isinstance(mcp, MyTau)
    assert mcp.type.h3

    ## Grab 3 MCParticle prongs
    mcp1, mcp2, mcp3 = mcp.children(CHARGED & HADRON)

    ## Standard (less-aggressive)
    find_similar = False
    l1 = self.find_reco_all(container, mcp1, find_similar=find_similar)
    l2 = self.find_reco_all(container, mcp2, find_similar=find_similar)
    l3 = self.find_reco_all(container, mcp3, find_similar=find_similar)
    ## Sort by overall-least variance
    triplets = zip_combine_tauh3(mcp1, mcp2, mcp3, l1, l2, l3)
    
    ## If fail, try again with experimental aggressive search
    if not triplets:
      find_similar = True
      l1 = self.find_reco_all(container, mcp1, find_similar=find_similar)
      l2 = self.find_reco_all(container, mcp2, find_similar=find_similar)
      l3 = self.find_reco_all(container, mcp3, find_similar=find_similar)
      triplets = zip_combine_tauh3(mcp1, mcp2, mcp3, l1, l2, l3)
      ## still fail, abort operation
      if not triplets:
        return None

    ## Combine trio into mother, may fail
    mother = self.combine(MCID(mcp), *(triplets[0]))
    if flag: # attach to given flag address
      mother.addInfo(flag, int(find_similar))
    return mother


  @lazy_algo_property
  def _list_tau_h3_true_mcpcand(self):
    """return list of set {mcp1,mcp2,mcp3}."""
    l = []
    for tau in self.list_tau:
      if tau.type.h3:
        trio = tau.children(CHARGED)
        l.append(set(trio))
    return l

  def _list_tau_xxx_true_mcpcand(self, ttype):
    l = []
    for tau in self.list_tau:
      if tau.type == ttype:
        l.append(tau.children(CHARGED)[0])
    return l

  @lazy_algo_property  ## Notice the caching
  def _list_tau_mu_true_mcpcand(self):
    return self._list_tau_xxx_true_mcpcand('mu')
    
  @lazy_algo_property
  def _list_tau_e_true_mcpcand(self):
    return self._list_tau_xxx_true_mcpcand('e')

  @lazy_algo_property
  def _list_tau_h1_true_mcpcand(self):
    return self._list_tau_xxx_true_mcpcand('h1')

    
  def mcMatch_ditau(self, ditaucand):
    ## Rely on mcMatch_tau, acting on both children of ditau
    if ditaucand.nChildren() != 2:
      logger.warning("Bad ditau candidate, shouldn't arrive here. Please recheck the code.")
      return False
    c1,c2 = ditaucand.children()
    return self.mcMatch_tau(c1) is not None and self.mcMatch_tau(c2) is not None

  def twoway_mcMatch_tau( self, ttype, recoTES ):
    """
    Helper method to yield pair of ( mcp, reco ) for efficiency & purity study.
    The result can be considered as table with 2 columns; some row has only mcp,
    whilst some has only reco. The row with both mcp-reco means they're matched.

    The procedure is following:

    1st-pass: Loop over all reconstructed particle in `recoTES`. For each of them, 
      try to find the mcp via `mcMatch_tau` method, or return None otherwise.
    
    2nd-pass: Loop over all mcp tau in the system. For each of them, yield only 
      the mcp that didn't get collected in 1st-pass.

    Note:
    - based on `mcMatch_tau` method above.
    - Only e/mu/h1 type is supported.

    """
    if ttype not in ( 'e', 'mu', 'h1' ):
      raise ValueError('Invalid ttype strategy, abort: %r'%ttype)

    ## 1st-pass, loop over reco
    found_mcp = defaultdict(list)  ## Use dict as unique set.
    for reco in self.selectTES( recoTES ):
      sreco = str(reco).strip()
      logger.debug('Checking: %s'%sreco)
      mcp = self.mcMatch_tau(reco)
      if mcp is not None:
        logger.debug('MCP -->: %s'%str(mcp).strip())
        ## Report for many-to-1 mcp-->reco 
        if mcp in found_mcp:
          logger.warning('many-to-1 mcp-->reco: %r'%found_mcp[mcp])
        # Hope for the hashing at instance-level...
        found_mcp[mcp].append( sreco )  
      yield mcp, reco

    ## 2nd-pass: Write all unmatched MCP
    for tau in self.list_tau:
      if tau.type == ttype:
        mcp = tau.children(CHARGED)[0]  # guaranteed single child
        if mcp not in found_mcp:
          logger.debug('MCP not picked: %s'%str(mcp).strip())
          yield mcp, None

  #-------------#
  # Trigger TOS #
  #-------------#

  @static_algo_property
  def FD_TOS_HIGHPTLEPTON(self):
    TOS_MUON0   = TOS('L0MuonDecision','L0TriggerTisTos')
    TOS_MUON1   = TOS('Hlt1SingleMuonHighPTDecision','Hlt1TriggerTisTos')
    TOS_MUON2   = TOS('Hlt2SingleMuonHighPTDecision','Hlt2TriggerTisTos')
    TOS_MUON    = TOS_MUON0 & TOS_MUON1 & TOS_MUON2

    TOS_ELECTRON0   = TOS('L0ElectronDecision','L0TriggerTisTos')
    TOS_ELECTRON1   = TOS('Hlt1SingleElectronNoIPDecision','Hlt1TriggerTisTos')
    TOS_ELECTRON2   = TOS('Hlt2SingleTFVHighPtElectronDecision','Hlt2TriggerTisTos')
    TOS_ELECTRON    = TOS_ELECTRON0 & TOS_ELECTRON1 & TOS_ELECTRON2

    d = {
      'TOS_MUON_L0'  : switch(ID==0, -1, switch( TOS_MUON0, 1, 0)),
      'TOS_MUON_HLT1': switch(ID==0, -1, switch( TOS_MUON1, 1, 0)),
      'TOS_MUON_HLT2': switch(ID==0, -1, switch( TOS_MUON2, 1, 0)),
      'TOS_MUON'     : switch(ID==0, -1, switch( TOS_MUON , 1, 0)),
      #
      'TOS_ELECTRON_L0'  : switch(ID==0, -1, switch( TOS_ELECTRON0, 1, 0)),
      'TOS_ELECTRON_HLT1': switch(ID==0, -1, switch( TOS_ELECTRON1, 1, 0)),
      'TOS_ELECTRON_HLT2': switch(ID==0, -1, switch( TOS_ELECTRON2, 1, 0)),
      'TOS_ELECTRON'     : switch(ID==0, -1, switch( TOS_ELECTRON , 1, 0)),
    }
    return d

  #------------------#
  # Custom Isolation #
  #------------------#

  def gen_isolation( self, reco, container='Phys/PFParticles' ):
    """
    Provide single-loop calculation for isolation.
    Do loop manually for the sake of same-BPV check. 
    Expected to be slower than functor SUMCONE.

    List of iso-quantity ( and their derivative )
    - PTCone05C; PTFrac05C
    - ECone05C ; EFrac05C
    - ECone05A ; EFrac05A
    - ECone02N, ECone05N ; EFrac02PN05N, EFrac02PN05A
    """
    if not reco:
      return {
        'NCone05C'    : -1.,
        'PTCone05C'   : -1.,
        'PTFrac05C'   : -1.,
        'ECone05C'    : -1.,
        'EFrac05C'    : -1.,
        'ECone05A'    : -1.,
        'EFrac05A'    : -1.,
        'ECone02N'    : -1.,
        'ECone05N'    : -1.,
        'EFrac02PN05N': -1.,
        'EFrac02PN05A': -1.,
      }

    NCone05C  = 0.
    ECone05A  = 0.
    ECone05N  = 0.
    ECone05C  = 0.
    PXCone05C = 0.
    PYCone05C = 0.
    ECone02N  = 0.

    Dr = DR2(reco)**0.5
    pv = self.bestPV(reco)
    Vsim      = Functions.VSIM( pv, minfrac = 0.7 )
    Hasprotos = HASPROTOS(reco)
    counter   = self.Counter('SameBPV for Isolation')

    for p in self.selectTES(container):
      dr = Dr(p)
      if dr > 0.5: # Skip immediately
        continue
      if Hasprotos(p): # Skip because it's contained in reco
        continue
      samebpv = Vsim(self.bestPV(p))
      # samebpv = (self.bestPV(p) == pv)
      counter += samebpv
      if not samebpv:  # Not from the same PV as core
        continue
      e = E(p) # energy
      ECone05A += e
      if Q(p) == 0: # Neutral
        ECone05N += e 
        if dr <= 0.2:  # InnerCone
          ECone02N += e       
      else:  # Charged
        ECone05C  += e 
        PXCone05C += PX(p)
        PYCone05C += PY(p)
        NCone05C  += 1

    ## Wrapup the quantities
    e  = E(reco)
    pt = PT(reco)
    PTCone05C     = (PXCone05C**2 + PYCone05C**2)**0.5
    PTFrac05C     = pt / ( pt + PTCone05C )
    EFrac05C      = e  / (  e + ECone05C  )
    EFrac05A      = e  / (  e + ECone05A  )
    EFrac02PN05N  = (e+ECone02N) / (e+ECone05N)
    EFrac02PN05A  = (e+ECone02N) / (e+ECone05A)
    return {
      'NCone05C'    : NCone05C, 
      'PTCone05C'   : PTCone05C,
      'PTFrac05C'   : PTFrac05C,
      'ECone05C'    : ECone05C,
      'EFrac05C'    : EFrac05C,
      'ECone05A'    : ECone05A,
      'EFrac05A'    : EFrac05A,
      'ECone02N'    : ECone02N,
      'ECone05N'    : ECone05N,
      'EFrac02PN05N': EFrac02PN05N,
      'EFrac02PN05A': EFrac02PN05A,
    }

  def gen_isolationMC( self, mcp ):
    """
    Like above, but done with MC particle. 
    Do just PTFrac05C, PTCone05C for now.
    """
    if not mcp:
      return { 'PTCone05C':-1., 'PTFrac05C':-1. }

    Dr = MCDR2(mcp)**0.5
    pv = mcp.primaryVertex()
    px = 0.
    py = 0.
    for neighbor in self.mcselect('neighbor', MCALL):
      if neighbor == mcp:
        continue
      if neighbor in mcp.descendants():
        continue
      if not CHARGED(neighbor):
        continue
      if neighbor.nChildren() > 0: 
        continue 
      if neighbor.primaryVertex() != pv:
        continue 
      if Dr(neighbor) > 0.5:
        continue   # skip
      px += MCPX(neighbor)
      py += MCPY(neighbor)
    PTCone05C = (px**2 + py**2)**0.5
    pt = MCPT(mcp)
    return {
      'PTCone05C': PTCone05C,
      'PTFrac05C': pt / (pt+PTCone05C)
    }
