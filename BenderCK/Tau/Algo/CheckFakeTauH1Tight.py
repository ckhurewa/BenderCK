#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from .. import MyTau
from ... import copyGoodEvents, writeEvent, MCABSID
from ...Decorators import count, pass_success

#==============================================================================

class CheckFakeTauH1Tight(BaseTauAlgoMC):
  """
  Given FAKE tau_h1 that passes identification, preselection, isolation cuts,
  debug the case where these guy have PTFrac05C==1

  Need DV_tau_cand_maker.py

  """

  Inputs = [ 'Phys/TightTau_h1' ]

  @staticmethod
  def init():  # Call be before appMgr()
    copyGoodEvents( filename = 'IsolatedFakeH1.dst', incident='IsolatedFakeH1' )
    copyGoodEvents( filename = 'PoorIsoTrueH1.dst' , incident='PoorisoTrueH1'  )

  @count
  def check_isolated_fake( self, reco ):
    print self.event_break
    print 'reco', str(reco).strip()
    mcp = self.find_mcp_best( reco )
    print 'mcp', str(mcp ).strip()
    
    ## 01. No MCP available, cannot continue
    if not mcp:
      return '01: No MCP available'
    
    ## 02. Has MCP, but has no MCP-mother, also cannot continue
    mcpmom = mcp.mother()
    if not mcpmom:
      return '02: No MCP-mom available'
    
    ## If mother is tau??
    if MCABSID(mcpmom)==15:
      mcptau = MyTau(mcpmom)
      print 'MCP-Tau', str(mcptau).strip()
    
      ## 03. This is one prong of the tau_h3:
      if mcptau.type.h3:
        return '03: Single-prong from tau_h3'
      return '-3: NotImplementedError'
    
    print 'mcp-ancestors', mcp.ancestors()
    return '-1: NotImplementedError'

  @count 
  def check_pooriso_true( self, reco ):
    pass

  @pass_success
  def analyse(self):
    for mcp,reco in self.twoway_mcMatch_tau( 'h1', 'TightTau_h1' ):
      if reco is not None:
        isostats = dict(self.gen_isolation( reco ))
        if mcp is None and isostats['PTFrac05C']==1:  # Fake cand
          writeEvent('IsolatedFakeH1' )  # For extraction
          # print self.check_isolated_fake( reco )
        elif mcp is not None and isostats['PTFrac05C'] < 0.4: # Poorly-iso true cand.
          writeEvent('PoorisoTrueH1')
          # self.check_pooriso_true( reco )

#==============================================================================
