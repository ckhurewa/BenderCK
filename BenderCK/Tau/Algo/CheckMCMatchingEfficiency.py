#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import ID, MCID, MCABSID, CHARGED, MC3Q, ABSID, MCETA, ETA, PHI, MCPHI, MCTREEMATCH
from ... import Functions
from ...Decorators import static_algo_property, count, pass_success

#==============================================================================

class CheckMCMatchingEfficiency(BaseTauAlgoMC):
  """
  Analyse the MC-matching efficiency before proceeding with selection.

  Because the key for good selection analysis is how we can distinguish between
  true and fake candidates using MC info, in order to study their reco props,
  It's thus necessary to refine the MCMatching process as accurate as possible.
  The role of MCMatching should simply return True/False whether the given cand
  is well-corresponds to its MC counterpart (process-dependent).

  In this particular case (Tau Identification), there are several non-mutually 
  exclusive approaches to get MCMatch:

  1. Use IP2MCP to find MC counterpart of given reco cand, then make sure this 
     MCP is in the desired tree. 
  - PRO: Entire property of MCP is available, solid, accurate.
  - CON: Poor support in PSF via TupleToolMCTruth (Need more work in Bender/ROOT).
  - CON: Required IP2MCP to yield corresponding MCP

  2. Using decay descriptor (e.g., [ tau- -> mu- ... ]CC), as well as other 
     functors in MCMatcherBase family
  - PRO: Simple syntax. 
  - PRO: Support in PSF (Particle Selection Framework)
  - CON: Found to be inaccurate, often over-match 
  - CON: VERY BUGGY in critical case (MCSELMATCH(MCMOTHCUT))

  3. Compare 4-vector directly between cand & mcp 
  - PRO: Simple & effective
  - PRO: Cover the ground where IP2MCP cannot find MCP counterpart.
  - CON: Badly not support in PSF.
  - CON: Unorthodox, eh?

  4. 

  This Algo will utilize all 3 strategies above, measure their accuracy,
  and develop the (probably) unified approach how to do mcmatching properly.
  Refering to classical errors, there're 2 types of error which we'll try 
  to minimize:

  - LOSS         : Candidate is reconstructed, but not matched.
  - CONTAMINATION: Fake candidate, but matching return True.

  Remark: This is measure in term of single particle, so 
  - tau_mu  --> mu 
  - tau_e   --> e 
  - tau_h1  --> pi+-
  - tau_h3  --> (ignore)
  """

  Inputs = [ 'Phys/PFParticles' ]

  @static_algo_property
  def p2mc(self):
    return self.tool('IP2MCP', 'MCMatchObjP2MCRelator')

  @count
  def report_bad(self, ok_mcp, ok_desc, ok_kine):
    if ok_mcp and ok_desc and ok_kine:
      return 'Success: Perfect match'
    if not (ok_mcp or ok_desc or ok_kine):
      return 'Success: Rejection'
    if ok_mcp and (not ok_desc) and (not ok_kine):
      return 'Failure: OK_MCP only'
    if (not ok_mcp) and (ok_desc) and (not ok_kine):
      return 'Failure: OK_desc only'
    if (not ok_mcp) and (not ok_desc) and (ok_kine):
      return 'Failure: OK_kine only'
    if (not ok_mcp) and (ok_desc) and (ok_kine):
      return 'Failure: All but MCP'
    if (ok_mcp) and (not ok_desc) and (ok_kine):
      return 'Failure: All but desc'
    if (ok_mcp) and (ok_desc) and (not ok_kine):
      return 'Failure: All but kine'

  def check_ok_mcp(self, cand, mcp):
    """Return True if this mcp (matched from reco) is a good pion (i.e., manual MCP decaydesc)"""
    if not mcp:
      return False 
    if MCID(mcp)!=ID(cand):  # The matching must yield same particle
      return False 
    mcp    = Functions.iTopCopyId(mcp)  # Remove recoil 
    mcpmom = mcp.mother()
    if not mcpmom:           # Non-null pointer to mother
      return False 
    if MCABSID(mcpmom)!=15:  # Mother is tau
      return False 
    if sum([ CHARGED(mcpmom[i+1]) for i in xrange(mcpmom.nChildren()) ])!=1:  # distinguish h1/h3
      return False 
    if MC3Q(mcpmom)!=MC3Q(mcp):    # Same sign tau & its child
      return False 
    return True

  @pass_success
  def analyse(self):
    queue = [
      ( 'h1', ABSID==211, 0.05, "[ tau- -> ^pi- {X0} {X0} {X0} {X0} {X0} ]CC" ),
      ( 'mu', ABSID==13 , 0.05, "[ tau- -> mu- ... ]CC" ),
      ( 'e' , ABSID==11 , 0.20, "[ tau- -> e-  ... ]CC" ),
    ]

    for ttype, candcut, threshold_norm, desc in queue:
      MATCHER = MCTREEMATCH(desc)

      ## Early abort, denominator is from IMCReconstructible.
      expected_taucand = []
      for tau in self.list_tau:
        if tau.type == ttype:
          mcp = tau.children(CHARGED)[0] 
          if self.isReconstructible(mcp):
            expected_taucand.append(mcp)
      if not expected_taucand:
        continue  # Check the next species

      ## Loop over cand
      for cand in self.selectTES('PFParticles'):
        if candcut(cand):
          self.report_bad.sharding = ttype
          mcp      = self.p2mc(cand) 
          ok_mcp   = self.check_ok_mcp(cand, mcp)
          ok_desc  = MATCHER(cand)
          ok_kine  = any(Functions.psimilar(truecand, cand, threshold_norm=threshold_norm) for truecand in expected_taucand)
          status   = self.report_bad( ok_mcp, ok_desc, ok_kine )
          if status.startswith('Failure'):
            print self.event_break
            print 'TType: ', ttype
            print status
            for tau in self.list_tau:
              if tau.type == ttype:
                print 'tau', tau
            for truecand in expected_taucand:
              print 'truecand', truecand
              print 'truecand: eta=%.03f, phi=%.03f' % (MCETA(truecand), MCPHI(truecand))
            print 'cand', cand
            print 'cand: eta=%.03f, phi=%.03f' % (ETA(cand), PHI(cand))
            print 'mcp (post-recoil)', mcp 
            if mcp:
              mcp    = Functions.iTopCopyId(mcp)  # Remove recoil 
              mcpmom = mcp.mother()
              if mcpmom:
                print 'mcpmom', mcpmom
                print 'sumchg', sum([ CHARGED(child) for child in mcpmom.children() ])
