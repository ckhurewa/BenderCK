#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import ID, MCID, MCABSID, CHARGED, ABSID, MCNINTREE, MCMOTHCUT, HADRON, PT, MCSELMATCH
from ... import Functions
from ...Decorators import static_algo_property, pass_success

#==============================================================================

class CheckMcselmatch(BaseTauAlgoMC):
  """
  Check the validity of MCSELMATCH functor, which is the crux of DV-based 
  candidate maker -- selection analysis.

  This Algo shows that given all those functors below are still INSUFFICIENT
  to do mcmatch single-pion as direct daughter of tau. There's always a 
  portion of non-direct pions get matched by these functors. 

  In real analysis, I need to evade this bug by using TupleToolMCTruth, 
  and re-apply cut at the ROOT level.
  """

  Inputs = [ 'Phys/PFParticles' ]

  @static_algo_property
  def p2mc(self):
    return self.tool('IP2MCP', 'MCMatchObjP2MCRelator')

  @pass_success
  def analyse(self):
    MCISTAU   = MCABSID==15
    MCISTAUH1 = (MCISTAU) & (MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON)==1)
    MCCUT     = MCSELMATCH(MCMOTHCUT( MCISTAUH1, False ))
    # MCCUT     = MCSELMATCH( (MCABSID==211) & (MCMOTHCUT( MCISTAUH1, False)) ) # & (~MCINANCESTORS(HADRON)) ) 
    # MCISTAUH3 = (MCABSID==15) & (MCNINTREE(MCMOTHCUT(MCABSID==15, False) & CHARGED & HADRON)==3)
    # MCCUT1    = MCSELMATCH((~MCINANCESTORS(HADRON)) & (MCABSID==211) & (MCMOTHCUT(MCABSID==15, False)))
    # MCCUT2 = MCTREEMATCH( "[ tau- ->  pi- {X0} {X0} {X0} {X0} {X0} ]CC" )


    ## Early abort
    expected_recocand = []
    for tau in self.list_tau:
      if tau.type.h1:
        for mcp in tau.children(CHARGED):
          # if self.isReconstructible(mcp):
          for cand in self.selectTES('PFParticles'):
            if ((ABSID==211) & (PT>100))(cand):
              if Functions.psimilar( mcp, cand ):
                expected_recocand.append( cand )
    if not expected_recocand:
      return


    ## Check for LOSS
    for truereco in expected_recocand:
      if not MCCUT(truereco):
        print self.event_break
        print '----- LOSS -----'
        print 'tau'
        for tau in self.list_tau:
          print tau
        print 'truereco', truereco
        mcp = self.p2mc(truereco)
        print 'mcp', mcp
        if mcp:
          print 'mcpmom', mcp.mother()
        # print 'MCCUT', MCCUT(cand)
        # print 'MCSELMATCH(MCABSID)'  , MCSELMATCH(MCABSID==211)(cand)
        # print 'MCSELMATCH(MCMOTHCUT)', MCSELMATCH(MCMOTHCUT( MCISTAUH1, False ))(cand)
        cname = 'Loss/' + ('MCP_found' if mcp else 'MCP_missing')
        counter = self.Counter(cname) 
        counter += 1

    ## Check for Contamination
    for cand in self.selectTES('PFParticles'):
      if ((ABSID==211) & (PT>100))(cand):
        if MCCUT(cand) and (cand not in expected_recocand):
          ## Split type
          mcp     = self.p2mc(cand)
          cname   = 'Contamination/'
          if not mcp:
            cname += 'missing MCP'
          elif ID(cand) != MCID(mcp):
            cname += 'misidentified MCID-ID'
          elif mcp.mother(): 
            mcpmom = mcp.mother()
            if ID(cand)==MCID(mcpmom):
              cname += 'failed motherid: recoil'
            elif MCABSID(mcpmom)!=15:
              cname += 'failed motherid'
            else:
              cname += 'other: reverse loss?'
          else:
            cname += 'other: no mother'
          ## Report
          print self.event_break
          print '----- ' + cname + ' -----'
          print 'tau'
          for tau in self.list_tau:
            print tau
          for reco in expected_recocand:
            print 'truereco', reco
          print 'cand', cand
          print 'cand-mccut', MCCUT(cand)
          print 'cand-mcp', mcp
          print 'cand-mcp-momcut', MCMOTHCUT( MCISTAUH1, False )(mcp)
          if mcp:
            mom = mcp.mother()
            print 'cand-mcpmom', mom
            if mom:
              print 'cand-mcpmom-cut', MCISTAUH1(mom)
              print 'cand-mcpmom-MCID', MCID(mom)
              print 'cand-mcpmom-istau', MCISTAU(mom)
              print 'cand-mcpmom-nprongs', MCNINTREE(MCMOTHCUT(MCISTAU, False) & CHARGED & HADRON)(mom)

          counter = self.Counter(cname)
          counter += 1

