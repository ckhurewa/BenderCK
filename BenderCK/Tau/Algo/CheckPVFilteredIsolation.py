#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ...Decorators import pass_success
from ... import Functors
from ... import PT, CHARGED, MCPT

class CheckPVFilteredIsolation(BaseTauAlgoMC):
  """
  Verify what changes for the new isolation where the neighbors is now filtered 
  to only those from same PV.

  If it's definitely better, it'll supercede the old version.

  Need DV_tau_cand_maker.
  """

  Inputs = [
    'Phys/TightTau_e',
    'Phys/TightTau_h1',
    'Phys/TightTau_mu',
  ]

  @pass_success
  def analyse(self):
    for cpath in self.Inputs:
      ttype = cpath.split('_')[-1]
      for mcp,reco in self.twoway_mcMatch_tau( ttype, cpath ):
        if reco is None: 
          continue  # Discard the pure-MC case
        MATCH = mcp is not None   # This means mcMatch with truth
        ## Alternative for unmatched reco
        if not MATCH:
          mcp = self.find_mcp_best( reco )
          if mcp is None:
            continue
        isostats    = self.gen_isolation(reco)
        isostats_mc = self.gen_isolationMC(mcp)
        with self.enhancedTuple( ttype ) as tup:
          tup.column( 'MATCH'         , MATCH   )
          tup.column( 'PT'            , PT(reco))
          tup.column( 'PTFrac05C_old' , Functors.PTFrac05C(reco) )
          tup.column( 'PTFrac05C_bad' , Functors.BAD_PTFrac05C(reco) )
          tup.column( 'PTCone05C_old' , Functors.PTCone05C(reco) )
          tup.column( 'PTFrac05C_new' , isostats['PTFrac05C']    )
          tup.column( 'PTCone05C_new' , isostats['PTCone05C']    )
          tup.column( 'PTFrac05C_mc'  , isostats_mc['PTFrac05C'] )
          tup.column( 'PTCone05C_mc'  , isostats_mc['PTCone05C'] )

    for tau in self.list_tau:
      if tau.type.h3:  
        continue  # Skip for now, complicate in isolation stage where I want just 3 pions, no neutrino
      mcp = tau.children(CHARGED)[0]
      isostats_mc = self.gen_isolationMC(mcp)
      with self.enhancedTuple( 'MC/'+tau.type ) as tup:
        tup.column( 'MCPT'     , MCPT(mcp) )
        tup.column( 'PTFrac05C', isostats_mc['PTFrac05C'] )
        tup.column( 'PTCone05C', isostats_mc['PTCone05C'] )
