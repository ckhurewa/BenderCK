#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ...Decorators import pass_success
from ... import Functions, LHCb
from ... import PT, IP, MIPDV, ETA, VVDIST, MCVDIST

#==============================================================================

class CheckPVRefit(BaseTauAlgoMC):
  """

  Verify the PV-refitting value and correctness with refitting is performed 
  from DaVinci algo. Make sure the PV,P2PV table is picked up correctly,
  compare the result, and hope memory issue is improved.

  Check the effect on IP (impact parameter) for single-particle, whilst FD 
  (flight distance) for decay particle. Use tau as model.

  Need something like the following at DV-level

  ## Manual fitting 

  >> algo = PVReFitterAlg('PVRefit_default')
  >> algo.UseIPVOfflineTool      = True
  >> algo.ParticleInputLocations = locations
  >> seq.Members.append(algo)

  ## Auto fitting 

  >> algo.ReFitPVs   = True
  >> algo.IgnoreP2PVFromInputLocations = True

  """

  Inputs = [
    ## IP only
    'Phys/TightTau_e',
    'Phys/TightTau_mu',
    'Phys/TightTau_h1',
    ## FD only
    'Phys/TightTau_h3',
  ]

  ## Dont use me, since it bias the bestPV command (and possibly Rec/Vertex/Best)
  ## Access the table directly
  # P2PVInputLocations = [


  # def compare_vertices(self):
  #   """
  #   Let's see how to let algo pick `P2PVInputLocations`. 
  #   Would it have better memory handling?
  #   """
  #   for reco in self.selectTES('Phys/TightTau_h1'):
  #     print self.event_break
  #     print reco 

  #     for tname in ['/Particle2VertexRelations', '/PVRefit_default_P2PV']:
  #       table = self.get( 'Phys/TightTau_h1'+tname )
  #       if table:
  #         print '## With table', tname
  #         for i,entry in enumerate(table.relations(reco)):
  #           vertex = entry.to()
  #           print 'Vertex', i, vertex.position(), 'IP', IP( vertex, self.geo() )(reco)
  #     print 'Related PV', self.getRelatedPV(reco).position()
  #     print 'BestPV', self.bestPV(reco).position()
  #     print '--Default-Primaries---'
  #     for i,v in enumerate(self.primaryVertices()):
  #       print 'PV', i, v.position()
      # print 'MIPTES', MIPTES('Phys/TightTau_h1/PVRefit_withIPVOfflineTool_PVs')(reco)
      # print 'MIPDV', MIPDV('', VALL)(reco)


  def analyse_IP( self, mcp, reco ):
    def Ip(table):
      if not table:
        return -1.
      l = table.relations(reco)
      if len(l)<1:
        return -1.
      return IP( l[0].to(), self.geo() )( reco )

    ## 0. Monte-Carlo exact value
    val0 = Functions.MCIP( mcp )

    ## 1. No-refit. Access the untouched PV
    ## 1A. Minimized value
    val1x = MIPDV('')(reco)  # MIPDV pick from default PVs container.
    val1a = min( IP(v,self.geo())(reco) for v in self.get('Rec/Vertex/Primary') )
    ## 1B. Best PV
    val1b = IP( self.bestPV(reco), self.geo() )(reco)

    print self.event_break
    print self.ttype, 'xab', val1x, val1a, val1b
    print '\nRec/Vertex/Primary'
    for v in self.get('Rec/Vertex/Primary'):
      print v.position(), IP(v,self.geo())(reco)
    print 'bestPV'
    print self.bestPV(reco).position()

    ## 2. Auto-refit
    val2a = Functions.MIP_P2PV( self.geo(), self.table2A, reco )
    val2b = Ip( self.table2B )

    ## 3/4. Manual refit via PVReFitterAlg
    val3a = Functions.MIP_P2PV( self.geo(), self.table3A, reco )
    val4a = Functions.MIP_P2PV( self.geo(), self.table4A, reco )

    val3b = Ip( self.table3B )
    val4b = Ip( self.table4B )

    ## Finally, write
    with self.enhancedTuple( self.ttype ) as tup:
      tup.column('nPVs'       , self.npv  )
      tup.column('PT'         , PT(reco)  )
      tup.column('ETA'        , ETA(reco) )
      tup.column('mc'                     , val0   )
      tup.column('norefit_min'            , val1a  )
      tup.column('norefit_best'           , val1b  )
      tup.column('autofit_min'            , val2a  )
      tup.column('autofit_best'           , val2b  )
      tup.column('manualfit_default_min'  , val3a  )
      tup.column('manualfit_default_best' , val3b  )
      tup.column('manualfit_pvo_min'      , val4a  )
      tup.column('manualfit_pvo_best'     , val4b  )

  def analyse_FD( self, mcp, reco ):
    Vvdist = VVDIST(reco.decayVertex())  ## Functor
    def minFD(table):
      return min( Vvdist(entry.to()) for entry in table.relations(reco) )
    def bestFD(table):
      #
      # print table.relations(reco)[0].to()
      #
      return Vvdist( table.relations(reco)[0].to() )

    ## 0. Monte-Carlo exact value
    vi    = mcp.primaryVertex()
    vf    = mcp.decayVertex()
    val0  = MCVDIST(vf)(vi)

    ## 1. No-Refit
    val1a = min( Vvdist(v) for v in self.get('Rec/Vertex/Primary') )
    val1b = Vvdist(self.bestPV(reco))

    # print self.event_break
    # print 'v1b', self.bestPV(reco)

    ## 2. Auto-refit
    ## 3. PVR default
    ## 4. PVR + PVO
    val2a = minFD ( self.table2A )
    val2b = bestFD( self.table2B )
    val3a = minFD ( self.table3A )
    val3b = bestFD( self.table3B )
    val4a = minFD ( self.table4A )
    val4b = bestFD( self.table4B )

    ## Finally, write
    with self.enhancedTuple( 'h3' ) as tup:
      tup.column('nPVs'       , self.npv  )
      tup.column('PT'         , PT(reco)  )
      tup.column('ETA'        , ETA(reco) )
      tup.column('mc'                     , val0   )
      tup.column('norefit_min'            , val1a  )
      tup.column('norefit_best'           , val1b  )
      tup.column('autofit_min'            , val2a  )
      tup.column('autofit_best'           , val2b  )
      tup.column('manualfit_default_min'  , val3a  )
      tup.column('manualfit_default_best' , val3b  )
      tup.column('manualfit_pvo_min'      , val4a  )
      tup.column('manualfit_pvo_best'     , val4b  )

  @pass_success
  def analyse(self):
    self.npv = self.get('Rec/Summary').info(LHCb.RecSummary.nPVs, -1)

    ## Abort if no PV (unbelievable, but possible)
    if len(self.get('Rec/Vertex/Primary'))==0:
      return

    for cpath in self.Inputs:
      self.ttype = cpath.split('_')[-1]

      ## From auto fitting
      self.table2A = self.get(cpath+'/Particle2VertexRelations')
      self.table2B = self.get(cpath+'/BestPVAlg_fromauto_P2PV')

      ## From manual fitting 
      ## no sort
      self.table3A = self.get(cpath+'/PVR_default_P2PV')
      self.table4A = self.get(cpath+'/PVR_withpvo_P2PV')
      ## sorted with BestPVAlg
      self.table3B = self.get(cpath+'/BestPVAlg_PVR_default_P2PV')
      self.table4B = self.get(cpath+'/BestPVAlg_PVR_withpvo_P2PV')

      do_fd = self.ttype=='h3'
      for reco in self.selectTES(cpath):
        ## Do only if I have MCP
        # mcp = self.find_mcp_best( reco )
        mcp = self.mcMatch_tau( reco )
        if not mcp:
          continue

        ## Separate the task here
        if do_fd:
          self.analyse_FD( mcp, reco )
        else:
          self.analyse_IP( mcp, reco )

#==============================================================================
