#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ...Decorators import pass_success
from ... import Functions, Functors
from ... import PRIMARY, CHARGED, BPVIP, IP, MINIP, MIPDV, MCPT

#==============================================================================

class CheckTauImpactParameter(BaseTauAlgoMC):
  """
  Prove that the calculation of sum SIP is correct at the reconstruction level,
  comparing against MC sample. This is crucial for the value of IPS in Z02TauTau
  analysis.
  """

  # Inputs = [ 
  #   # 'Phys/PFParticles',
  #   'Phys/StdAllNoPIDsPions',
  #   'Phys/StdAllLooseElectrons',
  #   'Phys/StdAllLooseMuons',
  # ]

  Container = {
    'e' : 'StdAllLooseElectrons',
    'mu': 'StdAllLooseMuons',
    'h1': 'StdAllNoPIDsPions',
  }


  def MIPSIG( self, *particles ):
    vertices = self.vselect( 'primary', PRIMARY )
    vertices = Functions.refits( self.defaultPVReFitter(), particles, vertices )
    return Functions.MIPSIG( self.geo(), particles, vertices )    

  def analyse_tau(self):

    for tau in self.list_tau:

      ## Write tau IP itself.
      with self.enhancedTuple('mcp/tau') as tup:
        tup.column( 'MCIP' , Functions.MCIP(tau)  )
        tup.column( 'MCSIP', Functions.MCSIP(tau) )

      ## Consider only e/h1/mu
      if tau.type.h3 or tau.type.other:
        continue

      ## Write the MC values
      mcp = tau.children(CHARGED)[0]
      with self.enhancedTuple('mcp/'+tau.type) as tup:
        tup.column( 'MCIP'    , Functions.MCIP(mcp)  )
        tup.column( 'MCSIP'   , Functions.MCSIP(mcp) )
      
      ## Build refit reco
      reco = self.find_reco_best( self.Container[tau.type], mcp )
      if not reco:
        continue

      # Refitting vertices
      vertices = self.vselected( 'Primary' )
      vertices_refitted = Functions.refits( self.primaryVertexReFitter('LoKi'), [reco], vertices )

      bpv_before      = self.bestPV(reco)
      bpvip_before    = BPVIP()(reco)
      if bpv_before:
        bpv_after       = Functions.refits( self.primaryVertexReFitter('LoKi'), [reco], [bpv_before] )[0]
        bpvip_after     = IP(bpv_after, self.geo())(reco)
      else:
        bpvip_after = -1.
      # bpvipchi2_after = IPCHI2(bpv, self.geo())(reco)
      # bpverr2_after   = bpvip_after**2/bpvipchi2_after if bpvipchi2_after>0 else -1.

      mip_before    = max( MIPDV()(reco), -1. ) # safetycap for tuple 
      mip_after     = max( MINIP(vertices_refitted, self.geo())(reco), -1.)
      # smip_before   = Functions.SMIP( self.distanceCalculator(), reco, vertices )
      # smip_after    = Functions.SMIP( self.distanceCalculator(), reco, vertices_refitted )
      # mipchi2_before = max( MIPCHI2DV()(reco), -1. )
      # mipchi2_after  = max( MIPCHI2(vertices_refitted, self.geo())(reco), -1.)
      # miperr2_after  = mip_after**2/mipchi2_after if mipchi2_after>0 else -1.
      # mipsig_manual = mip_after/miperr2_after**0.5 if (miperr2_after>0) else -1.
      # mipsig_auto   = self.MIPSIG( reco )
      # mipsig_auto   = Functions.MIPSIG( self.geo(), [reco], vertices_refitted )

      ## Write reco
      with self.enhancedTuple('tau/'+tau.type) as tup:
        # tup.RecoStats.fill()
        tup.column( 'MCPT', MCPT(mcp) )
        tup.fill_funcdict( Functors.FD_KINEMATIC, reco )
        tup.fill_funcdict( Functors.FD_PID      , reco )
        tup.fill_funcdict( Functors.FD_PROTO    , reco )
        tup.fill_funcdict( Functors.FD_TRACK    , reco )
        #
        tup.column( 'BPVIP_norefit'     , bpvip_before    )
        tup.column( 'MIP_norefit'       , mip_before      ) 
        tup.column( 'MIP'               , mip_after       )
        tup.column( 'BPVIP'             , bpvip_after     )

        # tup.column( 'MIPCHI2_norefit' , mipchi2_before  )
        # tup.column( 'SMIP_norefit'    , smip_before     )
        # tup.column( 'SMIP'            , smip_after      )
        # tup.column( 'MIPCHI2'         , mipchi2_after   )
        # tup.column( 'MIPERR2'         , miperr2_after   )
        # tup.column( 'MIPSIG_manual'   , mipsig_manual   )
        # tup.column( 'MIPSIG'          , mipsig_auto     ) # For cross-check 


        # tup.column( 'TRCHI2'          , TRCHI2(reco)            )
        # tup.column( 'TRUNCER2'        , Functors.TRUNCER2(reco) ) # At Last-measurement state.
        #   # tup.column( 'BPVDIRA'     , BPVDIRA(reco) )  # -2000
        #   # tup.column( 'BPVPATHDIST' , BPVPATHDIST(reco) ) # 0 Pathdistance
        #   # tup.column( 'BPVPROJDIST' , Functors.BPVPROJDIST(reco) ) # -1E10

  def analyse_ditau(self):


    for ditau in self.list_ditau:
      ## Skip h3/other
      if 'h3' in ditau.type or 'other' in ditau.type:
        continue

      ## Write MC
      mcp1 = ditau.tau1.children(CHARGED)[0]
      mcp2 = ditau.tau2.children(CHARGED)[0]
      with self.enhancedTuple('mcp/'+ditau.type) as tup:
        tup.column( 'MCIP1'     , Functions.MCIP(mcp1)  )
        tup.column( 'MCIP2'     , Functions.MCIP(mcp2)  )
        tup.column( 'MCSIP1'    , Functions.MCSIP(mcp1) )
        tup.column( 'MCSIP2'    , Functions.MCSIP(mcp2) )
        
      ## Prepare reco
      p1      = self.find_reco_best( self.Container[ditau.tau1.type], mcp1 )
      p2      = self.find_reco_best( self.Container[ditau.tau2.type], mcp2 )
      valid   = (p1 is not None) and (p2 is not None)
      counter = self.Counter(ditau.type)
      counter += int(valid)
      if not valid:
        continue

      ## Refitting vertices
      vertices = self.vselected( 'Primary' )
      vertices_refitted = Functions.refits( self.primaryVertexReFitter('LoKi'), [p1,p2], vertices )

      # bpv_before      = self.bestPV(reco)
      # bpvip_before    = BPVIP()(reco)
      # if bpv_before:
      #   bpv_after       = Functions.refits( self.primaryVertexReFitter('LoKi'), [reco], [bpv_before] )[0]
      #   bpvip_after     = IP(bpv_after, self.geo())(reco)
      # else:
      #   bpvip_after = -1.


      ## Calculation
      # bpvip1_before   = BPVIP()(p1)
      # bpvip2_before   = BPVIP()(p2)
      # mip1_before     = max( MIPDV(PRIMARY)(p1), -1. )
      # mip2_before     = max( MIPDV(PRIMARY)(p2), -1. )
      # smip1_before    = Functions.SMIP( self.distanceCalculator(), p1, vertices )
      # smip2_before    = Functions.SMIP( self.distanceCalculator(), p2, vertices )
      # #
      # mip1_after      = max( MINIP(vertices_refitted, self.geo())(p1), -1.)
      # mip2_after      = max( MINIP(vertices_refitted, self.geo())(p2), -1.)
      # mipchi2_1_after = max( MIPCHI2(vertices_refitted, self.geo())(p1), -1.)
      # mipchi2_2_after = max( MIPCHI2(vertices_refitted, self.geo())(p2), -1.)
      # smip1_after     = Functions.SMIP( self.distanceCalculator(), p1, vertices_refitted )
      # smip2_after     = Functions.SMIP( self.distanceCalculator(), p2, vertices_refitted )
      # #
      # truncer2_1    = Functors.TRUNCER2(p1)
      # truncer2_2    = Functors.TRUNCER2(p2)
      # #
      # err2          = ((mip1_after**2/mipchi2_1_after) + (mip2_after**2/mipchi2_2_after))
      # mipsig_manual = abs(smip1_after+smip2_after)/err2**0.5 if (err2>0) else -1.
      # mipsig_auto   = self.MIPSIG( p1, p2 )

      mipsig_before  = Functions.MIPSIG( self.geo(), [p1,p2], vertices )
      mipsig_after   = Functions.MIPSIG( self.geo(), [p1,p2], vertices_refitted )

      with self.enhancedTuple('ditau/'+ditau.type) as tup:
        tup.column( 'MIPSIG_norefit'  , mipsig_before )
        tup.column( 'MIPSIG'          , mipsig_after  ) 
        for prefix,reco in [ ('p1',p1), ('p2',p2) ]:
          tup.fill_funcdict( Functors.FD_KINEMATIC, reco, prefix=prefix )
          tup.fill_funcdict( Functors.FD_PID      , reco, prefix=prefix )
          tup.fill_funcdict( Functors.FD_PROTO    , reco, prefix=prefix )
          tup.fill_funcdict( Functors.FD_TRACK    , reco, prefix=prefix )
          tup.column(prefix+'_MIP', max( MINIP(vertices_refitted, self.geo())(reco), -1.))

        # tup.column( 'BPVIP1_norefit'  , bpvip1_before   )
        # tup.column( 'BPVIP2_norefit'  , bpvip2_before   )
        # tup.column( 'MIP1_norefit'    , mip1_before     )
        # tup.column( 'MIP2_norefit'    , mip2_before     )
        # tup.column( 'SMIP1_norefit'   , smip1_before    )
        # tup.column( 'SMIP2_norefit'   , smip2_before    )
        # tup.column( 'MIP1'            , mip1_after      )
        # tup.column( 'MIP2'            , mip2_after      )
        # tup.column( 'MIPCHI2_1'       , mipchi2_1_after )
        # tup.column( 'MIPCHI2_2'       , mipchi2_2_after )
        # tup.column( 'SMIP1'           , smip1_after     )
        # tup.column( 'SMIP2'           , smip2_after     )
        # tup.column( 'TRCHI2_1'        , TRCHI2(p1)      )
        # tup.column( 'TRCHI2_2'        , TRCHI2(p2)      )
        # tup.column( 'TRUNCER2_1'      , truncer2_1      )
        # tup.column( 'TRUNCER2_2'      , truncer2_2      )
        #

  @pass_success
  def analyse(self):
    self.vselect( 'Primary', PRIMARY )
    self.analyse_tau()
    self.analyse_ditau()

#==============================================================================
