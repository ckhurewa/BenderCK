#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ...Decorators import static_algo_property, pass_success
from ... import Functors
from ... import PT

#==============================================================================

class CompareIso(BaseTauAlgoMC):
  """
  Solely focus on the difference between my (naive) isolation functors and
  TupleToolConeIsolation. 
  """

  ## Configure before appMgr
  from Configurables import TupleToolConeIsolation
  conf = TupleToolConeIsolation('CompareIso.TupleToolConeIsolation')
  conf.MinConeSize = 0.5
  conf.MaxConeSize = 0.5
  conf.OutputLevel = 10

  @static_algo_property
  def cone_isolation(self):
    return self.tool('IParticleTupleTool', 'TupleToolConeIsolation')

  @pass_success
  def analyse(self):
    for reco in self.get('Phys/StdAllNoPIDsPions/Particles'):
      if PT(reco) > 4E3:
        with self.enhancedTuple('iso') as tup:
          tup.fill_funclist( ['P','PT']           , reco)
          tup.fill_funcdict( Functors.FD_ISOLATION, reco )
          self.cone_isolation.fill( reco, reco, 'tt', tup )

#==============================================================================
