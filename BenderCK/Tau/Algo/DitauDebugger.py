#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import LHCb, Functions, Functors, get, ROOT
from ... import CHARGED, MCETA, MCPHI
from ...Decorators import static_algo_property, pass_success

#==============================================================================

class DitauDebugger(BaseTauAlgoMC):
  """
  Provide every information needed to debug the MC-level ditau.
  Contain 2 tuples: 
  - No-weight: To approximate the BR of sample.
  - Re-weight: To balance the filesize evenly across ditau final states 
               ( e.g., prescale on h1h1 channel since it's largest, against mumu)
  """

  def __init__(self, reweight=True):
    """Optionally turn on/off reweighting."""
    self.reweight = reweight
    super(DitauDebugger, self).__init__()

  ## IMCReconstructed
  @static_algo_property
  def MCR(self):
    return self.tool('IMCReconstructed', 'MCReconstructed')
  # def reconstructed(self, mcp):
  #   return int(self._MCReconstructed.reconstructed(mcp))
  # def isReconstructed(self, mcp):
  #   return self.reconstructed(mcp) > 0

  @pass_success
  def analyse(self):
    ## no-reweight
    for ditau in self.list_ditau:
      with self.enhancedTuple('ditau') as tup:
        tup.column( 'itype', ditau.itype )

    ## static
    nSPDhits  = get('Rec/Summary').info(LHCb.RecSummary.nSPDhits, -1)
    nPVs      = get('Rec/Summary').info(LHCb.RecSummary.nPVs    , -1)
    nTracks   = get('Rec/Summary').info(LHCb.RecSummary.nTracks , -1)

    ## Weighted by BR (unless disable)
    queue = self.list_ditau_weighted if self.reweight else self.list_ditau
    for ditau in queue:
      ## Prep variables      
      tau1bot = ditau.tau1
      tau2bot = ditau.tau2
      tau1top = Functions.iTopCopyId(tau1bot)
      tau2top = Functions.iTopCopyId(tau2bot)
      tau1ch  = tau1bot.vec4_children_charged  # 4Vector-sum of charged prong
      tau2ch  = tau2bot.vec4_children_charged

      phistar_taubot = Functions.phistar( MCETA(tau1bot), MCETA(tau2bot), MCPHI(tau1bot), MCPHI(tau2bot)  )
      phistar_tauch  = Functions.phistar( tau1ch.Eta()  , tau2ch.Eta()  , tau1ch.Phi()  , tau2ch.Phi()    )

      ## Tuple with reweight
      with self.enhancedTuple( ditau.type ) as tup:
        tup.column_uint( 'tau1_itype'  , tau1bot.itype  )
        tup.column_uint( 'tau2_itype'  , tau2bot.itype  )
        #
        tup.fill_funcdict( Functors.FD_MCKINEMATICS, ditau    , prefix='ditau'   )
        tup.fill_funcdict( Functors.FD_MCKINEMATICS, tau1top  , prefix='tau1top' )
        tup.fill_funcdict( Functors.FD_MCKINEMATICS, tau2top  , prefix='tau2top' )
        tup.fill_funcdict( Functors.FD_MCKINEMATICS, tau1bot  , prefix='tau1bot' )
        tup.fill_funcdict( Functors.FD_MCKINEMATICS, tau2bot  , prefix='tau2bot' )
        #
        tup.column( 'DR'            , ROOT.Math.VectorUtil.DeltaR( tau1ch, tau2ch ))
        tup.column( 'PhiStar_taubot', phistar_taubot )
        tup.column( 'PhiStar_tauch' , phistar_tauch  )
        #
        tup.column( 'tau1ch_P'    , tau1ch.P()  )
        tup.column( 'tau1ch_PT'   , tau1ch.Pt() )
        tup.column( 'tau1ch_M'    , tau1ch.M()  )
        tup.column( 'tau1ch_ETA'  , tau1ch.Eta())
        tup.column( 'tau1ch_PHI'  , tau1ch.Phi())
        #
        tup.column( 'tau2ch_P'    , tau2ch.P()  )
        tup.column( 'tau2ch_PT'   , tau2ch.Pt() )
        tup.column( 'tau2ch_M'    , tau2ch.M()  )
        tup.column( 'tau2ch_ETA'  , tau2ch.Eta())
        tup.column( 'tau2ch_PHI'  , tau2ch.Phi())
        #
        tup.column( 'ditau_ch_M'  , (tau1ch+tau2ch).M())
        tup.column( 'ditau_ch_PT' , (tau1ch+tau2ch).Pt())
        #
        tup.column_uint( 'nPVs'    , nPVs )
        tup.column_uint( 'nTracks' , nTracks )
        tup.column_uint( 'nSPDhits', nSPDhits )
        #
        # Only for simple type here for quick study
        if tau1bot.type in ('e','mu','h1'):
          p = tau1bot.children(CHARGED)[0]
          tup.column_int( 'tau1ch_REC', int(self.MCR.reconstructed(p)))
        if tau2bot.type in ('e','mu','h1'):
          p = tau2bot.children(CHARGED)[0]
          tup.column_int( 'tau2ch_REC', int(self.MCR.reconstructed(p)))

