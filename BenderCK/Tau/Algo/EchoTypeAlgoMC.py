#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import logger
from ...Decorators import count, real_passthrough_success

#==============================================================================

class EchoTypeAlgoMC(BaseTauAlgoMC):
  """
  Simply read all tau/ditau, and report its type. 
  """

  #-----#
  # TAU #
  #-----#

  @count 
  def count_tau_type(self, tau):
    return tau.type
  @count
  def count_tau_mother_name(self, tau):
    return tau.mother_name
  @count 
  def count_tau_other(self, tau):
    if tau.type.other:
      logger.debug(tau)
      return str(tau._ci_absids)

  #-------#
  # DITAU #
  #-------#

  @count 
  def count_ditau_type(self, ditau):
    return ditau.type
  @count 
  def count_ditau_name(self, ditau):
    return ditau.name()

  @real_passthrough_success
  def analyse(self):
    for tau in self.list_tau:
      self.count_tau_type(tau)
      self.count_tau_mother_name(tau)
      self.count_tau_other(tau)
    for ditau in self.list_ditau:
      self.count_ditau_type(ditau)
      self.count_ditau_name(ditau)
