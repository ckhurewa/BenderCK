#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import logger, Functions, CHARGED, MCETA, MCP, MCPT, MCPHI, MCABSID
from ...Decorators import count, real_passthrough_success

#==============================================================================

class TauChildrenSeparationAlgoMC(BaseTauAlgoMC):
  """
  Report the separation between (interesting) children of tau, at MC level.
  Groupped by tau.type

  - e     : None 
  - h1    : pi/pi0,  
  - h3    : pi/pi0, pi/pi, 3pi
  - mu    : None
  - other : None
  """

  @count
  def check_duo_separation(self, nickname, tau, mcp1, mcp2):
    ## Need slightly more args just for logging's sake.
    dr = Functions.MCDR(mcp1,mcp2)
    logger.debug(self.event_break)
    logger.debug('DR: %.3f  | (eta,phi) = (%.2f,%.2f), (%.2f,%.2f)'%(dr,MCETA(mcp1),MCPHI(mcp1),MCETA(mcp2),MCPHI(mcp2)))
    logger.debug(tau)
    logger.debug(mcp1)
    logger.debug(mcp2)
    ## Make ntuple
    tname = tau.type+'_'+nickname
    self.check_duo_separation.sharding = tname
    with self.enhancedTuple(tname) as tup:
      tup.column('MCDR'     , dr)
      tup.column('p1_MCP'   , MCP (mcp1))
      tup.column('p1_MCPT'  , MCPT(mcp1))
      tup.column('p2_MCP'   , MCP (mcp2))
      tup.column('p2_MCPT'  , MCPT(mcp2))
      tup.column('tau_MCP'  , MCP (tau))
      tup.column('tau_MCPT' , MCPT(tau))
    return dr

  @count
  def check_trio_separation(self, tau):
    c1,c2,c3 = tau.children(CHARGED)
    DRs      = Functions.MCDR_trio(c1, c2, c3)
    PTs      = sorted([ MCPT(c1), MCPT(c2), MCPT(c3) ])
    logger.debug(self.event_break)
    logger.debug('DRs: {:.3f}, {:.3f}, {:.3f}'.format(*DRs))
    logger.debug(tau)
    ## Make ntuple
    with self.enhancedTuple('tau_h3') as tup:
      tup.column('MCDR_min' , DRs[0] )
      tup.column('MCDR_mid' , DRs[1] )
      tup.column('MCDR_max' , DRs[2] )
      tup.column('MCDR_mean', sum(DRs)/3)
      tup.column('MCPT_min' , PTs[0] )
      tup.column('MCPT_mid' , PTs[1] )
      tup.column('MCPT_max' , PTs[2] )
      tup.column('MCPT_mean', sum(PTs)/3)
      tup.column('tau_MCP'  , MCP(tau))
      tup.column('tau_MCPT' , MCPT(tau))
      tup.column('acute'    , DRs[2]**2 < DRs[0]**2+DRs[1]**2 ) # Check if it's acute triangle.
    self.check_trio_separation.sharding = 'tau_h3_DRmean'
    return sum(DRs)/3

  @real_passthrough_success
  def analyse(self):
    for tau in self.list_tau:

      ## Study the duo pair
      if tau.type.h1 or tau.type.h3 :
        list_pi  = tau.children(CHARGED)
        list_pi0 = tau.children(MCABSID==111)
        ## Report pi/pi0
        for pi,pi0 in itertools.product(list_pi, list_pi0):
          self.check_duo_separation('pipi0', tau, pi, pi0)
        ## Report pi/pi 
        for pi1,pi2 in itertools.combinations(list_pi, 2):
          self.check_duo_separation('pipi', tau, pi1, pi2)

      ## Extra for h3
      if tau.type.h3:
        self.check_trio_separation(tau)
