#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import logger, MCID, CHARGED
from ...Decorators import count, pass_success

#==============================================================================

class TauFinalChildrenMigrationAlgoMC(BaseTauAlgoMC):
  """
  Simply report the significant type migration.
  Defined as the type of tau's final state children, compare the inital type.
  Ignore all neutral particles.
  """

  @count
  def report_migration(self, tau, l1, l2):
    logger.debug(self.event_break)
    logger.debug(tau)
    logger.debug(l1)
    logger.debug(l2)
    s1 = str(sorted([ int(MCID(p)) for p in l1 ])).replace(' ','')
    s2 = str(sorted([ int(MCID(p)) for p in l2 ])).replace(' ','')
    s  = s1+' -> '+s2
    return s

  @pass_success
  def analyse(self):
    for tau in self.list_tau:
      l1 = [ p for p in tau._children_initial if CHARGED(p) ]
      l2 = [ p for p in tau._children_final   if CHARGED(p) ]
      if l1 != l2:
        self.report_migration(tau, l1, l2)

