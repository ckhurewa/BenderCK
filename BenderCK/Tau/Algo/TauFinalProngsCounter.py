#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import CHARGED, HADRON
from ...Decorators import pass_success

#==============================================================================

class TauFinalProngsCounter(BaseTauAlgoMC):
  """
  Question: Given tau_h1/tau_h3, what are the eventual effective number of prongs?
  """
  @pass_success
  def analyse(self):
    for tau in self.list_tau:
      if tau.type.h1 or tau.type.h3:
        nfinal  = len(tau.children(CHARGED&HADRON, final=True))
        cname   = str(tau.type) + '/' + str(nfinal)
        counter = self.Counter(cname)
        counter += 1

        ## Print out a bit, for 3 prongs
        if nfinal==3:
          print self.event_break
          print tau 
          print tau.children(CHARGED&HADRON, final=True)

