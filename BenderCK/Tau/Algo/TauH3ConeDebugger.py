#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import Functions, CHARGED, MCPT, MCETA
from ...Decorators import pass_success

#==============================================================================

class TauH3ConeDebugger(BaseTauAlgoMC):
  """
  Focus on the study of MCDR-vs-MCPT of tau_h3.
  i.e., the opening angle of inner-cone, formed by 3-prongs pions,
  as a function of MCPT (usually taken as some of 3 prongs,exclude neutrino).

  ## Below feature are not included (relevant, but reco-dependent)
  - Also against reco-matched DR-vs-PT 
  - The intra-event comb-background is also included, albeit prescaled.
  """

  @pass_success
  def analyse(self):
    for tau in self.list_tau:
      if tau.type.h3:
        drs     = Functions.MCDR_trio(*tau.children(CHARGED))
        with self.enhancedTuple('tau_h3') as tup:
          tup.column( 'MCDR_min'        , drs[0] )
          tup.column( 'MCDR_mid'        , drs[1] )
          tup.column( 'MCDR_max'        , drs[2] )
          tup.column( 'MCDR_mean'       , sum(drs)/3.)
          tup.column( 'MCPT'            , MCPT(tau))
          tup.column( 'MCETA'           , MCETA(tau))
          tup.column( 'MCPT_charged'    , tau.vec4_children_charged.Pt() )
          tup.column( 'MCETA_charged'   , tau.vec4_children_charged.Eta() )
