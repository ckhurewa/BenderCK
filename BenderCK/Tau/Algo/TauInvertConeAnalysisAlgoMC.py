#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import logger, CHARGED, E, DR2, SUMCONE, SUCCESS, Q
from ...Decorators import pass_success

#==============================================================================

class TauInvertConeAnalysisAlgoMC(BaseTauAlgoMC):
  """
  Focus on the invert-cone problem. (h1/h3 only)

  - Use PF-based reco as combinatorics background

  EFrac  = 70%
  PTFrac = 70%

  Q1: Fixing outer = 0.5 PFCHARGED, what is the radius of inner neutral cone needed?
  Q2: Fixing inner = 3prongs, what max outer-chargedcone-radius possible?


  # >>> TODO; Show the neutral-cone for h1 exists, then use this info to 
  # make static/dynamic cone for candidates selection.


  """

  # @count
  # def calculate_invert_cone_efrac70(self, comb):
  #   """
  #   Return minimum outer radius such that the energy-fraction of inner/outer ~ 0.7
  #   """
  #   children = sorted([ comb(1), comb(2), comb(3) ], key=PT, reverse=True) # Hardest first
  #   func_dr  = lambda p: DR2(children[0])(p)**0.5
  #   einner   = sum(E(x) for x in children) 
  #   eouter   = float(einner)
  #   # for neig in sorted(self.get('Phys/PFParticles/Particles'), key=func_dr):
  #   for neig in sorted(self.selectTES('PFParticles'), key=func_dr):
  #     if neig not in children and Q(neig)!=0:  # Outer is charged only...
  #       dr      = func_dr(neig)
  #       eouter += E(neig)
  #       efrac   = einner/eouter
  #       # logger.info('DR=%.3f CumEFrac=%.3f  | %s' % (dr, efrac, str(neig).strip().split('\n')[0]))
  #       if efrac < 0.7:
  #         return dr
  #   return 99.  # Absolute dominance


  def determine_invert_cone_h1(self, mcptau):

    logger.debug(self.event_break)
    logger.debug('Start invert cone check')
    logger.debug(str(mcptau).strip())

    if mcptau.type == 'h1':
      mcpprong  = mcptau.children(CHARGED)[0]
      recoprong = self.find_reco_best(mcpprong)
      if not recoprong:
        return 
    elif mcptau.type == 'h3':
      self.clear()
      for mcppi in mcptau.children(CHARGED):
        recopi = self.find_reco_best(mcppi)
        if recopi:
          logger.debug('Saving: '+str(recopi).strip())
          self.save('prong', recopi)
      all_rawcomb = list(self.loop('prong prong prong', 'tau-'))
      print all_rawcomb
      return

    e_prong   = E(recoprong)
    func_dr   = DR2(recoprong)**0.5 

    q1_einner = 0. # running
    q1_eouter = SUMCONE( 0.5**2, E, 'Phys/PFParticles', Q!=0 )(recoprong)
    q2_einner = e_prong 
    q2_eouter = 0. # running
    q1_result = None
    q2_result = None

    logger.debug(str(recoprong).strip())
    logger.debug('Q1: einner: %.2f' % q1_einner)
    logger.debug('Q1: eouter: %.2f' % q1_eouter)
    logger.debug('Q2: einner: %.2f' % q2_einner)
    logger.debug('Q2: eouter: %.2f' % q2_eouter)

    # Note: Need dr=0 as initial condition
    for neig in sorted(self.selectTES('PFParticles'), key=func_dr):
      if q1_result is not None and q2_result is not None:
        break
      e_neig    = E(neig)
      dr        = func_dr(neig)
      is_charge = Q(neig)!=0
      logger.debug('Testing neighbor: DR=%.2f, E=%.2f' % (dr,e_neig))
      if q1_result is None and (dr==0 or (not is_charge)):
        q1_einner += e_neig
        q1_eouter += e_neig  # Add neutral particle to outer sum too.
        if q1_einner / q1_eouter > 0.7:
          q1_result = dr 
          logger.debug('Found RQ01: %.2f'%dr)
      if q2_result is None and (dr==0 or is_charge):
        q2_eouter += e_neig
        if q2_einner / q2_eouter < 0.7:
          q2_result = dr 
          logger.debug('Found RQ02: %.2f'%dr)
    return { 'RQ01': q1_result, 'RQ02': q2_result }

  @pass_success
  def analyse(self):
    for tau in self.list_tau:
      if tau.type in ('h1','h3'):
        results = self.determine_invert_cone_h1(tau)
        if results:
          vec4cc = tau.vec4_children_charged
          with self.enhancedTuple('tau_'+tau.type) as tup:
            tup.fill_dict(results)
            tup.column('MCPT_charged' , vec4cc.Pt() )
            tup.column('MCETA_charged', vec4cc.Eta() )

    self.setFilterPassed(True)
    return SUCCESS

