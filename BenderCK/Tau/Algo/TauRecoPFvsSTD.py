#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import Functors, ABSID, PT
from ...Decorators import pass_success

#==============================================================================

class TauRecoPFvsSTD(BaseTauAlgoMC):
  """
  Definite Algo to prove whether PF or STD container is more suitable to use 
  as a container for tau candidate.

  Unlike TauRecoStats, this will rely on identification & preselection cut, 
  so the output will thus be an ntuple available for cutting in ROOT later. 
  (And that's also why this chosed not to be added as a part of TauRecoStats).

  - Ignore tau_h3 for now, implies things from tau_h1
  """

  Inputs = [
    'Phys/StdAllLooseMuons',
    'Phys/StdAllNoPIDsPions',
    'Phys/StdAllNoPIDsElectrons',
    'Phys/PFParticles',
  ]

  def preselected( self, ttype, reco ):
    if not reco:
      return False
    if ABSID(reco) not in (11,13,15,211,321):
      return False
    return PT(reco)>4000
    # raise NotImplementedError

  def write( self, tname, mcp, reco ):
    hasMC   = mcp is not None 
    hasRECO = reco is not None
    with self.enhancedTuple(tname) as tup:
      tup.column       ( 'IS_MC'     , self.is_MC        )
      tup.column       ( 'EVTYPE'    , self.event_type   )
      tup.column       ( 'MATCH'     , hasMC and hasRECO )
      tup.column       ( 'hasMC'     , hasMC             )
      tup.column       ( 'hasRECO'   , hasRECO           )
      tup.fill_gen     ( self.tau_event_summary          )
      #
      tup.fill_funcdict( Functors.FD_MCKINEMATICS, mcp   )
      tup.fill_funcdict( Functors.FD_MCPID       , mcp   )
      #
      tup.fill_funcdict( Functors.FD_KINEMATIC   , reco  )
      tup.fill_funcdict( Functors.FD_ISOLATION   , reco  )
      tup.fill_funcdict( Functors.FD_TRACK       , reco  )
      tup.fill_funcdict( Functors.FD_PROTO       , reco  )
      tup.fill_funcdict( Functors.FD_PID         , reco  )

  @pass_success
  def analyse(self):
    ttype = 'mu'
    for cnickname, cname in [('PF','PFParticles'), ('STD','StdAllLooseMuons')]:
      tname = ttype+'_'+cnickname
      for mcp,reco in self.twoway_mcMatch_tau( ttype, cname ):
        ## Apply preselection to reduce filesize
        if not self.preselected( ttype, reco ):
          reco = None
        ## Discard empty entry
        if mcp is None and reco is None:
          continue
        ## Finally, write to tuple
        self.write( tname, mcp, reco )

