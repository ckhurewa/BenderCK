#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .BaseTauAlgoMC import BaseTauAlgoMC
from ... import logger, CHARGED, E, ID, PT, ETA, MCALL, MCID, MCPT, MCETA, MCE, MCPHI
from ...Decorators import real_passthrough_success, count, static_algo_property

#==============================================================================

class TauRecoStats(BaseTauAlgoMC):
  """
  This algorithm reports the reconstruction efficiency and MC-matching 
  (given MC-truth sample) of a tau-based analysis.

  This heavily relies on IMCReconstructible & MCMATCH.
  """

  CONTAINERS = {
    'e': (
      'PFParticles',
      'StdAllNoPIDsElectrons',
      'StdAllLooseElectrons',
      'StdLooseANNElectrons',
      'StdTightANNElectrons',
    ),
    'mu': (
      'PFParticles',
      'StdAllLooseMuons',
      'StdAllVeryLooseMuons',
      'StdTightMuons',
      'StdTightANNMuons',
    ),
    'pi': (
      'PFParticles',
      'StdAllNoPIDsPions',
      'StdAllLoosePions',
      'StdLoosePions',
      'StdTightPions',
      'StdAllLooseANNPions',
      'StdTightANNPions',
    ),
    'pi0': (
      'StdLooseMergedPi0',
      'StdLooseResolvedPi0',
      'StdLooseDalitzPi0',
    ),
  }

  def __init__(self, ntuple=False):
    self._ntuple = ntuple
    inputs = list({ 'Phys/'+tes for l in self.CONTAINERS.values() for tes in l })
    inputs.append('StdLooseDetachedTau3pi')  # For 3-prongs check
    super(TauRecoStats, self).__init__(Inputs=inputs)

  @static_algo_property
  def tt_reco_stats(self):
    return self.tool('IEventTupleTool', 'TupleToolRecoStats')

  #-----#
  # TAU #
  #-----#

  def count_tau_children_inacc(self, tau):
    return sum(self.inAcceptance(child) for child in tau.children(CHARGED))
  def count_tau_children_recoa(self, tau):
    return sum(self.isReconstructible(child) for child in tau.children(CHARGED))
  def count_tau_children_recod(self, tau):
    return sum(self.isReconstructed(child) for child in tau.children(CHARGED))
  def all_tau_children_recodPF(self, tau):
    return all(self.find_reco_best('PFParticles', child) is not None for child in tau.children(CHARGED))

  @count
  def count_tau_children_recoTES(self, tau, tes):
    return sum(len(self.find_reco_all(tes, child))>0 for child in tau.children(CHARGED))

  @count 
  def tau_fully_inacc(self, tau):
    self.tau_fully_inacc.sharding = str(self.event_type)+'/'+tau.type
    return self.count_tau_children_inacc(tau) == len(tau.children(CHARGED))
  @count 
  def tau_fully_reconstructible(self, tau):
    self.tau_fully_reconstructible.sharding = str(self.event_type)+'/'+tau.type
    return self.count_tau_children_recoa(tau) == len(tau.children(CHARGED))
  @count 
  def tau_fully_reconstructed(self, tau):
    self.tau_fully_reconstructed.sharding = str(self.event_type)+'/'+tau.type
    return self.count_tau_children_recod(tau) == len(tau.children(CHARGED))
  @count 
  def tau_fully_reconstructedPF(self, tau):
    self.tau_fully_reconstructedPF.sharding = str(self.event_type)+'/'+tau.type
    return self.all_tau_children_recodPF(tau)


  def export_tau_ntuple(self, tau):
    ttype = tau.type
    with self.enhancedTuple('TauReco_'+ttype) as tup:
      # self.tt_reco_stats.fill(tup)  ## Buggy
      tup.column( 'EVTYPE'            , self.event_type )
      tup.column( 'itype'             , tau.itype )
      tup.column( 'nChildren_charged' , len(tau.children(CHARGED)) )
      tup.column( 'MCPT'              , MCPT(tau))
      tup.column( 'MCETA'             , MCETA(tau))
      tup.column( 'MCPHI'             , MCPHI(tau))
      tup.column( 'MCPT_charged'      , tau.vec4_children_charged.Pt() )
      tup.column( 'MCETA_charged'     , tau.vec4_children_charged.Eta() )
      tup.column( 'MCPHI_charged'     , tau.vec4_children_charged.Phi() )
      tup.column( 'count_child_INACC' , self.count_tau_children_inacc(tau) )
      tup.column( 'count_child_RECOA' , self.count_tau_children_recoa(tau) )
      tup.column( 'count_child_RECOD' , self.count_tau_children_recod(tau) )
      teskey = ttype if ttype in self.CONTAINERS else 'pi'
      for tes in self.CONTAINERS[teskey]:
        func          = self.count_tau_children_recoTES
        func.sharding = str(self.event_type)+'/'+ttype+'/'+tes
        tup.column('count_child_%s'%tes, func(tau, tes))        


  #-------#
  # DITAU #
  #-------#

  @count 
  def ditau_fully_inacc(self, ditau):
    self.ditau_fully_inacc.sharding = str(self.event_type)+'/'+ditau.type
    return all(self.count_tau_children_inacc(tau) == len(tau.children(CHARGED)) for tau in ditau)

  @count 
  def ditau_fully_reconstructible(self, ditau):
    self.ditau_fully_reconstructible.sharding = str(self.event_type)+'/'+ditau.type
    return all(self.count_tau_children_recoa(tau) == len(tau.children(CHARGED)) for tau in ditau)

  @count 
  def ditau_fully_reconstructed(self, ditau):
    self.ditau_fully_reconstructed.sharding = str(self.event_type)+'/'+ditau.type
    return all(self.count_tau_children_recod(tau) == len(tau.children(CHARGED)) for tau in ditau)

  @count 
  def ditau_fully_reconstructedPF(self, ditau):
    self.ditau_fully_reconstructedPF.sharding = str(self.event_type)+'/'+ditau.type
    return all(self.all_tau_children_recodPF(tau) for tau in ditau)

  #--------#
  # TAU_H3 #
  #--------#

  # @count
  # def count_tau_h3_PF_success(self, tau):
  #   """Report how many prongs were successfully caught by PF, from [0,3]"""
  #   # return sum(self.find_reco_best(pi) is not None for pi in tau.children(CHARGED))
  #   count = 0
  #   for mcp in tau.children(CHARGED):
  #     func   = self.new_matcher(mcp)
  #     count += any(func(p) for p in self.selectTES('PFParticles'))
  #   return count


  # @count # (ntuple=True)
  # def count_tau_h3_Std_success(self, tau):
  #   """Report how many prongs were successfully caught by any of the StdLooseDetachedTau3pi, from [0,3]"""
  #   def match_any_prong(p):
  #     return any(self.new_matcher(trueprong)(p) for trueprong in tau.children(CHARGED))
  #   def count_match(cand):
  #     # max 3
  #     return sum(match_any_prong(p) for p in cand.children())
  #   list_cand = self.selectTES('StdLooseDetachedTau3pi') # list(self.get('Phys/StdLooseDetachedTau3pi/Particles'))
  #   if list_cand.empty():
  #     return 0
  #   return max(count_match(cand) for cand in list_cand)


  @count
  def report_tau_child_reco(self, ttype, ptype , mcp):
    """
    Interest in the reco class for each tau's children.
    As well as the matching difference between MCP/P by PF
    """

    ## Pre-determine me first
    code  = self.reconstructible(mcp)
    text  = self._MCReconstructible.text(code)
    shard = '{}/{}: {}'.format(ttype, ptype, text)
    logger.debug('Classification: '+shard)
    self.report_tau_child_reco.sharding = shard

    ## Begin find (study each TES)
    any_valid = False
    tname     = 'TauChildReco__{}_{}'.format(ttype, ptype)
    with self.enhancedTuple(tname) as tup:
      tup.column('IMCReconstructed_CODE'  , self.reconstructed(mcp))
      tup.column('IMCReconstructible_CODE', code)
      tup.column('EVTYPE' , self.event_type )
      tup.column('MCID'   , MCID(mcp))
      tup.column('MCPT'   , MCPT(mcp))
      tup.column('MCE'    , MCE(mcp))
      tup.column('MCETA'  , MCETA(mcp))
      ## Study each reco matches quality for each MCP
      for tes in self.CONTAINERS.get(ptype, []):
        reco      = self.find_reco_best(tes, mcp)
        valid     = reco is not None
        any_valid |= valid
        strreco   = '---' if not valid else str(reco).strip().replace(' '*20,'') 
        logger.debug('{ttype}/{ptype} @ {tes:30}: {strreco}'.format(**locals()))
        # Persist tuple
        tup.column(tes+'_VALID', int(valid) )
        tup.column(tes+'_ID'   , ID(reco)  if valid else   0. )
        tup.column(tes+'_PT'   , PT(reco)  if valid else -99. )
        tup.column(tes+'_E'    , E(reco)   if valid else -99. )
        tup.column(tes+'_ETA'  , ETA(reco) if valid else -99. )
    ## Finally, return count
    return any_valid

  @real_passthrough_success
  def analyse(self):

    ## Tau loop
    logger.debug(self.event_break)
    for tau in self.list_tau:
      logger.debug(tau)

      ## 1. Report counters
      self.tau_fully_inacc(tau)
      self.tau_fully_reconstructible(tau)
      self.tau_fully_reconstructed(tau)
      self.tau_fully_reconstructedPF(tau)

      # if tau.type.h3: # tau-h3 is more special
      #   self.count_tau_h3_PF_success(tau)
      #   self.count_tau_h3_Std_success(tau)

      if self._ntuple:
        ## 2. Export nTuple
        self.export_tau_ntuple(tau)
        ## 3. Detailed report on single child of tau
        for ptype,child in tau.iterchildren(MCALL):
          self.report_tau_child_reco(tau.type, ptype, child)

    ## Ditau loop
    for ditau in self.list_ditau:
      logger.debug(ditau)
      self.ditau_fully_inacc(ditau)
      self.ditau_fully_reconstructible(ditau)
      self.ditau_fully_reconstructed(ditau)
      self.ditau_fully_reconstructedPF(ditau)
