#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Collections of simple+useful Algo for Tau/Ditau study
See each class for more detail.

"""

from BaseTauAlgoMC import BaseTauAlgoMC
from CheckFakeTauH1Tight import CheckFakeTauH1Tight
from CheckMCMatchingEfficiency import CheckMCMatchingEfficiency
from CheckMcselmatch import CheckMcselmatch
from CheckPVFilteredIsolation import CheckPVFilteredIsolation
from CheckPVRefit import CheckPVRefit
from CheckTauImpactParameter import CheckTauImpactParameter
from CompareIso import CompareIso
from DitauDebugger import DitauDebugger
from EchoTypeAlgoMC import EchoTypeAlgoMC
from TauChildrenSeparationAlgoMC import TauChildrenSeparationAlgoMC
from TauFinalChildrenMigrationAlgoMC import TauFinalChildrenMigrationAlgoMC
from TauFinalProngsCounter import TauFinalProngsCounter
from TauH3ConeDebugger import TauH3ConeDebugger
from TauInvertConeAnalysisAlgoMC import TauInvertConeAnalysisAlgoMC
from TauRecoPFvsSTD import TauRecoPFvsSTD
from TauRecoStats import TauRecoStats
