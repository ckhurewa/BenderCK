#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .MyTau import MyTau
from ... import LHCb, Functions, logger, MCABSID
from ...Decorators import lazy_property

#==============================================================================

class MyDitau(LHCb.MCParticle):
  """
  Wrapper around the Ditau MCParticle with useful properties/methods

  - Iterate over MyDitau to yield its children, instance of MyTau.
  - Report decay classification from MyDitau.type

  """
  
  _tau1 = None 
  _tau2 = None

  TYPES = {
    'e_e'     :  0,
    'e_h1'    :  1,
    'e_h3'    :  2,
    'e_mu'    :  3,
    'e_other' :  4,
    'h1_h1'   : 11,
    'h1_h3'   : 12,
    'h1_mu'   : 13,
    'h1_other': 14,
    'h3_h3'   : 22,
    'h3_mu'   : 23,
    'h3_other': 24,
    'mu_mu'   : 33,
    'mu_other': 34,
    'other_other': 44,
  }


  def __init__(self, mcp):
    super(MyDitau, self).__init__(Functions.iBotCopyId(mcp).clone())
    # Careful: can have more than 2 children (e.g., gamma)
    l = [ c for c in self.children() if MCABSID(c)==15 ]
    if len(l)!=2:
      raise ValueError('Bad ditau!\n'+str(mcp))
    logger.debug('MyTau candidates: %r'%l)
    l = sorted([MyTau(c) for c in l])  # sort by incresing ttype, decreasing PT
    self._tau1  = l[0]
    self._tau2  = l[1]

  # Loop over 2 MyTau using generator protocol
  def __iter__(self):
    for tau in [ self._tau1, self._tau2 ]:
      yield tau

  #-----------------------------------------------

  @property 
  def tau1(self):  # Note, already a MyTau instance
    return self._tau1

  @property 
  def tau2(self):
    return self._tau2

  @lazy_property
  def type(self):
    l = [ tau.type for tau in self ]
    return '_'.join(sorted(l))
  
  @property
  def itype(self):
    """Return the indexed type (from e_e as 0, e_h1 as 1, ...)"""
    return self.TYPES.get(self.type, -1)

  @property
  def is_type_MuX(self):
    """Return True if it should be caught by MuXLine"""
    return 'mu' in self.type

  @property
  def is_type_EX(self):
    """Return True if it should be caught by EXLine"""
    return 'e' in self.type

  @lazy_property
  def vec4_children_charged(self):
    """Call `vec4_children_charged` on each tau and return the vectorial sum"""
    return self.tau1.vec4_children_charged + self.tau2.vec4_children_charged
