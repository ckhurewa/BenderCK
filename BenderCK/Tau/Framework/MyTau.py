#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter
from PythonCK.itertools import EnumContainer
from PythonCK.decorators import report_debug

from ... import (
  LHCb, Functions, ROOT, logger, 
  CHARGED, MCABSID, MCPT, MCALL, HADRON,
)
from ...Decorators import lazy_property

#==============================================================================

class MyTau(LHCb.MCParticle):
  """
  Helper class which facilitate the study of Tau lepton.

  ### Simple (single) type 

  This is the class of tau where it decays to one-charged prong:

  - tau -> e  ( 17.83% )
  - tau -> mu ( 17.41% )
  - tau -> pi ( 46.92% )

  This exclude, for now, the 3-prongs channel (required more care).

  ### Nomenclature: CORE-CHILDREN

  This *core-children* refer to all children which are usually reconstructed
  for study at LHCb. In this case, they are electrons, muons, and charged pions.

  Remark: Some tau may not have decay... Strange...

  """

  __slots__ = ()

  TYPES = EnumContainer('e', 'h1', 'h3', 'mu', 'other')

  def __init__(self, mctau):
    ## First check: Can only be instantiate from TAU
    assert MCABSID(mctau)==15, 'Need LHCb.MCParticle instance of Tau!, got %r'%mctau
    # Use iBotCopyId to unsure that we can study it
    bot_mctau  = Functions.iBotCopyId(mctau)
    self._hash = bot_mctau.key()  # Before it gets destroyed by init
    super(MyTau, self).__init__(bot_mctau.clone())
    self._validate()  # Autocheck

  def __hash__(self):
    """To uniquize the same tau being recoiled. Rely on iBotCopyId"""
    return self._hash

  def __eq__(self,other):
    """Need together with __hash__"""
    return hash(self)==hash(other)

  def __lt__(self, other):
    """For sorting, first by type (alphabetic order), then by *DECREASING* MCPT"""
    #then by CHILD's highest MCPT (decreasing)"""
    if self.type != other.type:
      return self.type < other.type  # increasing alphab 
    return MCPT(self) > MCPT(other)  # Decreasing PT
    # l1 = self._core_children
    # l2 = other._core_children
    # if l1 and l2:
    # return self._core_children[0] > other._core_children[0]

  @lazy_property
  @report_debug
  def _children(self):
    """
    TRY TO Override the faulty one! Return all MC children in this MC-tau:

    - Resort by descending MCPT.
    - Still have the neutrinii.

    """
    return sorted([self[i+1] for i in xrange(self.nChildren()) ], key=MCPT, reverse=True)


  @lazy_property
  @report_debug
  def _children_initial(self):
    """
    Initial list of children (to determine intrinsic type). 
    
    - EXCLUDE NEUTRINII HERE!
    - Still include some neutral particles.
    
    """
    return [ p for p in self._children if MCABSID(p) not in (12,14,16) ]

  @lazy_property
  @report_debug
  def _ci_absids(self):
    return sorted([ int(MCABSID(p)) for p in self._children_initial ])    

  @lazy_property
  @report_debug
  def _children_final(self):
    """
    Final state of children of this tau.
    Sort it by decreasing MCPT.
    """
    def _children_rcsv(list_p):
      for p in list_p:
        if p.nChildren()==0:
          yield p 
        else:
          for c in _children_rcsv([p[i+1] for i in range(p.nChildren())]):
            yield c
    return sorted(list(_children_rcsv(self._children_initial)), key=MCPT, reverse=True)


  def _validate(self):

    # ## It should decays!
    # if self.nChildren()==0:
    #   logger.warning('Tau does not decay!?')
    #   logger.warning(self)
    #   raise ValueError

    ## electron/muon/pion should be MUTUALLY EXCLUSIVE
    s = set(self._ci_absids)
    if {11,13} in s or {11,211} in s or {13,211} in s:
      msg = 'Found electron/muon/pion as mutual children. FIXME!'
      logger.warning(msg)
      logger.warning(self)
      logger.warning(self._children_initial)
      raise ValueError(msg)

    ## Count of charged pion/kaons should be odd (or 0)
    # c   = Counter(self._ci_absids)
    # val = c[211]+c[321]+c[213]  # pi + K + rho(770)
    val =  len(self.children(CHARGED & HADRON))
    if val>0 and val%2==0:
      msg = 'Even number of charged hadron prongs!? FIXME!'
      logger.warning()
      logger.warning(self)
      logger.warning(self._children_initial)
      raise ValueError(msg)


    # # Internal check once at __init__
    # set_core_children_absid   = set(self._core_children_absid)
    # if any([
    #   {11,13} <= set_core_children_absid,
    #   # {11,211} <= set_core_children_absid,
    #   # {13,211} <= set_core_children_absid,
    #   len(set_core_children_absid)==0,
    #   len(self._core_children_absid)==2,
    # ]):
    #   logger.warning('self: %r'%self)
    #   logger.warning('core_children: %r'%self._core_children)


  @lazy_property
  @report_debug
  def type(self):
    """
    Return one of the 5 (string) tau type.
    - Use _children directly instead of core, to be more precise.
    - Try to get type as intrinsic as possible (i.e., if pi+ decays to muons, considered it as pi0)
    - Validation if already early-checked (e.g., electron+muon simultaneously) 
    - Begin ignore the neutral particle at this point.

    Careful of strange but possible type, like tau -> eee
    """ 

    if self.nChildren()==0:
      return MyTau.TYPES.other

    count_hprong    = 0
    count_electron  = 0
    count_muon      = 0
    for p in self._children_initial:
      if not CHARGED(p):
        continue
      absid = MCABSID(p)
      if absid == 11:
        count_electron += 1
      elif absid == 13:
        count_muon += 1
      else:
        count_hprong += 1

    if count_muon == 1:
      if not count_electron and not count_hprong:
        return MyTau.TYPES.mu
      else:
        return MyTau.TYPES.other
    elif count_muon > 1:
      return MyTau.TYPES.other

    if count_electron == 1:
      if not count_muon and not count_hprong:
        return MyTau.TYPES.e
      else:
        return MyTau.TYPES.other
    elif count_electron > 1:
      return MyTau.TYPES.other

    ## at this point, e == mu == 0

    if count_hprong == 1:
      return MyTau.TYPES.h1
    elif count_hprong == 3:
      return MyTau.TYPES.h3
    elif count_hprong in (5,7,9):  # Considered as other
      return MyTau.TYPES.other 
      # return 'h5'
    else:
      logger.warning(count_hprong)
      logger.warning(self._children_initial)
      logger.warning(self)
      raise NotImplementedError('Unidentified tau type.')

  @lazy_property
  def itype(self):
    """Return numeric enum of tau's type. e==0, h1=1, ..."""
    return self.TYPES.index(self.type)

  @lazy_property
  def list_hprong_initial(self):
    """Try to return the list of hadron prongs in the initial children."""
    return [ p for p in self._children_initial if CHARGED(p) and MCABSID(p) not in (11,13 )]


  @lazy_property
  def mother_name(self):
    try:
      return Functions.iTopCopyId(self).mother().name()
    except ReferenceError as e:
      logger.warning(repr(e))
      return 'N/A'

  def children(self, cuts=MCALL, final=False):
    ## OVERRIDE, with enchancement on cutting
    if final:
      return [ mcp for mcp in self._children_final if cuts(mcp)]
    else:
      return [ mcp for mcp in self._children if cuts(mcp)]

  # @property
  # def nChildrenCharged():
  #   """Thsi is used very often, so better cached it."""
  #   return len()

  def iterchildren(self, cuts=CHARGED, uniquekey=False):
    """
    Just like `iteritems` in dict, yield (key,child) for iteration. With following touches:
    - Yield K-,Pi- as the same key (it's considered similarily as charge hadron track)
    - Plus/minus sign are ignored.

    Args:
      (bool) uniquekey   : If True, the key will be the same of each specie type (useful when 
                           treating the prongs different way, especially in tau_h3).
                           If False, for each duplicate prong, it'll be suffixed with count.
                           (e.g., pi,pi2,pi3 ).
      (LoKi.Functor) cuts: LoKi functor to apply on the returning children. Default to (MC)CHARGED
      (bool) initial_only: NotImplementedError

    - Guarantee to have unique key to use for tupe (useful for 3prongs case, for example.)
    """

    @report_debug
    def safename(p):
      """Return the safe/simplified name used in tau's namespace."""
      # sid = int(str(int(MCABSID(p)))[-3])
      mcabsid = int(MCABSID(p))
      if mcabsid==11:
        return 'e'
      elif mcabsid==13:
        return 'mu'
      elif mcabsid==22:
        return 'gamma'
      return 'pi' if CHARGED(p) else 'pi0'

    used_name = Counter()
    for p in self._children_initial:
      if cuts(p):
        key = safename(p)
        if uniquekey:
          used_name[key] += 1
          val = used_name[key]
          if val > 1:
            key += str(val)
        yield key,p

  @lazy_property
  def vec4_children_charged(self):
    """
    Return vectorial sum of tau's charged children (for RecoStats).
    This is ROOT's 4-vector.
    """
    psum = ROOT.Math.LorentzVector("ROOT::Math::PxPyPzE4D<double>")()
    for child in self.children(CHARGED):
      psum += child.momentum()
    return psum 

