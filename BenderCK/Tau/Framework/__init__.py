#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

General framework to for Tau analysis.

Useful class-wrapper on LHCb.MCParticle

"""

from MyTau import MyTau
from MyDitau import MyDitau
