#!/usr/bin/env python

from . import *
from .Functors import *       # For fill_funcdict
from LoKiCore.math import *   # For eval of func.

#==============================================================================

_iett = [
  'RecoStats', 
  'EventInfo',
  'Stripping',
]
_iptt = [
  'Pid', 
  'Geometry',
]  # List only those which are safe to ignore mother in fill's arg

def __getattr__(self, name):
  if (name in _iett) or (name in _iptt):
    cachekey  = '__tool_'+name
    tool      = globals().get(cachekey, None)  ## Cache at the global level
    if not tool:
      # log.debug('Init tool: '+cachekey)
      if name in _iett:
        interface = 'IEventTupleTool' 
        _filler   = lambda: cpp.IEventTupleTool.fill(tool, self)
      elif name in _iptt: 
        interface = 'IParticleTupleTool'
        _filler   = lambda p,head='': cpp.IParticleTupleTool.fill( tool, None, p, head, self )
      else:
        raise AttributeError
      tool = self._algo.tool(interface, 'TupleTool%s'%name)
      #
      ## Twist the method `fill` of IEventTupleTool to have `tup` as default arg.
      tool.fill = _filler  #._old_fill(self)  # New call interface for fill, argless.
      #
      ## Cache me
      globals()[cachekey] = tool
      ## Cleanup
      import atexit
      atexit.register(lambda: globals().pop(cachekey))
    return tool
  raise AttributeError("%r object has no attribute %r" % (self.__class__, name))



def fill_funcdict(self, d, particle, prefix='', default=-1E12):
  """
  Shortcut method to quickly fill the particle against dict of functors.
  """
  ## Auto add '_' after prefix
  if prefix and not prefix.endswith('_'):
    prefix += '_'
  for key,func in sorted(d.iteritems()):
    if isinstance(func, basestring):
      func = eval(func)
    self.column(prefix+key, func(particle) if particle else default) # null-support, use with care


def fill_funclist(self, l, particle, prefix='', default=-1E12):
  """
  Shortcut method to fill list of functor-string.
  """
  if prefix and not prefix.endswith('_'):
    prefix += '_'
  for key in sorted(l):
    func = eval(key)
    self.column(prefix+key, func(particle) if particle else default)


def fill_gen(self, gen, prefix=''):
  """
  Quickly fill the generator type into tuple. 
  Expect yield of type (basestring,number)

  Note: Depreciate dict type insertion because in most use cases, 
  creating a new dict inside a large-loop is NOT optimal.
  """
  if prefix and not prefix.endswith('_'):
    prefix += '_'
  if isinstance(gen, dict):  # Extra support for dict class
    gen = gen.iteritems()
  for key,val in gen:
    self.column(prefix+key, val)
