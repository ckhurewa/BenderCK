
__author__  = 'Chitsanu Khurewathanakul'
__email__   = 'chitsanu.khurewathanakul@gmail.com'
__license__ = 'GNU GPLv3'

## Usual libs
import os
import sys
sys.path.insert(0, os.path.join(os.getcwd(),'PythonCK.zip'))  # For Bender@Ganga

## Other often use libraries
from glob import glob as _glob_old
glob = lambda x: sorted(_glob_old(x))  # Sorted please!

## Import PythonCK (no invocation in child)
import PythonCK
from PythonCK import logger
from PythonCK.decorators import *

if 'BENDERSYSROOT' in os.environ:
  ## Usual env
  import ROOT
  from Bender.Main import *   
  from Bender.MainMC import *
  from BenderTools.GoodEvents import copyGoodEvents, writeEvent
  from Configurables import DaVinci  # Beyond common

  # http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d1/da5/namespace_bender_tools_1_1_good_events.html

  ## PATCHING imports -- Be careful with these
  # Do early because of MCDR2
  from __patcher__ import *

else:
  logger.warning('No Bender environment available...')

  # ## For sphinx documentation.
  # from mock import Mock as Mock0
  # class Mock(Mock0):
  #   @classmethod
  #   def __getattr__(cls, name):
  #     return Mock()
  # globals()['cpp'] = Mock()
  # globals()['AlgoMC'] = object
  # for func in ['MCALL','CHARGED']:
  #   globals()[func] = func
  # globals()['LHCb'] = type('LHCb', (), {'MCParticle': object})
  # globals()['importOptions'] = Mock()
  # globals()['appMgr'] = Mock()
  # globals()['SUCCESS'] = Mock()
  # MOCK_MODULES = ['Configurables',]
  # sys.modules.update((mod_name, Mock()) for mod_name in MOCK_MODULES)


## Early-patch
## Missing Functors...
MCDR2 = cpp.LoKi.MCParticles.DeltaR2

# ## Non-namespace-polluting import
# import Functions 
# import Functors  # Do late import. Many functor required desktop (context).

## Populate namespace
from BaseConfigure import *
from Decorators import *  

## Fixes the Bender's strange default
import __builtin__
sum = __builtin__.sum

## Finally, customize the log
# logger.reinit_blacklist()
