#!/usr/bin/env python

## Those will be overriding the previous vars in global context.
# This will affect `from __patcher__ import *` in __init__
__all__ = (
  'Algo', 
  'AlgoMC',
)

from . import cpp, LHCb

from . import Tuple
cpp.Tuples.Tuple.fill_funcdict  = Tuple.fill_funcdict
cpp.Tuples.Tuple.fill_funclist  = Tuple.fill_funclist
cpp.Tuples.Tuple.fill_gen       = Tuple.fill_gen


# cpp.Tuples.Tuple.__getattr__    = Tuple.__getattr__ # Still buggye
## In CheckImpactParamTool, many tuples were made, but only one of them
## has called the IEET



from . import BaseAlgo
Algo   = BaseAlgo.NewBaseAlgo
AlgoMC = BaseAlgo.NewBaseAlgoMC

from . import Aux
LHCb.MCParticle.__str__   = Aux.MCParticle__str__( LHCb.MCParticle.__str__  )
#
LHCb.Particle.__add__     = Aux.Particle__add__ ( LHCb.Particle.__add__     )
LHCb.Particle.__radd__    = Aux.Particle__radd__( LHCb.Particle.__radd__    )
LHCb.MCParticle.__add__   = Aux.Particle__add__ ( LHCb.MCParticle.__add__   )
LHCb.MCParticle.__radd__  = Aux.Particle__radd__( LHCb.MCParticle.__radd__  )

# LHCb.MCParticle.__str__ = Aux._mcparticle__str__(LHCb.MCParticle.__str__, shorten=True)
# LHCb.Particle.__str__   = Aux._mcparticle__str__(LHCb.Particle.__str__  , shorten=False)
