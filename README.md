BenderCK
========

Improving `Bender` for better productivity.

Most works are focused on algorithms to study tau lepton at LHCb,
including object-oriented tau/di-tau classes (derived from `LHCb.MCParticle`).


## Dependency

This should be used inside LHCb-`Bender` environment.

## Disclaimer

This packacge was written and used during my PhD in 2013-2017 at EPFL (Lausanne) and LHCb collaboration (CERN),
for the work in *Z->tau tau* cross-section measurement and *H->mu tau* searches at LHCb (8TeV).
