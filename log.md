
> Exclude the case of tau- -> e- e- e+ nu_e~ nu_tau to "other"

> Put MCREC in DitauDebugger as well. Simple enough for reco eff calculation
  for cross-section study


> 160120: Revisiting MC selection of ditau & classification.
  Aim: I relied enough on Bender now. Try to use tool as standard (LoKi) as 
  possible in order to gradually get back to DV-based ntuple.
  Also, this may help uncover the problem with cross-section of ditaumumu.
  > First, try to classify dtype (0,1,2,3) using only functor.

> Possibility for decaydesc testing framework ??
  - find SmartRefVector  --> ROOT.SmartRefVector('LHCb::Particle')
  - Its element: ROOT.SmartRef('LHCb::Particle')
  - Thus, CLS1().push_back(CLS2(p))
  - http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/brunel/releases/v37r0/doxygen/d5/df4/_check_8py-source.html
  > Halt for now....


================================================================================
                              BENDER-CK LIBRARY
================================================================================


- Refactored BaseAlgo to be less clustered.

### MyBender ---> BenderCK

- New *enhancedTuple* framework:
  - Motivation: Use Bender's ntuple in python's context-style `with` statement.
    This will allow instantiation of tuple in limited scope, provide the `write`
    method upon exit (in case you forget), as well as other auxiliary method 
    which can simplify the ntuple-persistence process.
  - `tuple.fill_dict`: For particle + dict of functor.
  - `tuple.fill_gen`: Narrow the use case to generator-based tuple-filling. 
    Dict is too expensive to create. Also avoid method overloading in 
    performance-demanding situation (again, inspired by golang). 
    Method-overloading is better for interactive-driven situation.
    - usage: `tup.fill_gen(my_generator)`
  - Lazy access to `IEventTupleTool` / `IParticleTupleTool` ? 
    - Call `tup.EventInfo.fill()` to ask instance of `IEventTupleTool` to fill
      this specific ntuple with its fill method. The tool should be cached 
      alongside the lifecycle of this ntuple's respective Algo instance.
    - The interface use-cases is very narrow, fetching correct interface from 
      implemented class should be simple, via `cppyy`.
    - Um, question, how can a tuple instantiate a tool?
      - Use contextmanager (with)? such that the tuple holds pointer to its Algo.
      - Yeah! This works!
      - BECAREFUL with memory leakage!
    - All's well, except strange `IMCParticleTupleTool` which needsworkaround
      - Mailed to Bender mailinglist. No  response...
  - Support dictionary very natively?
- I need new `lazy_algo_method` decorator
  - Motivated from PF/StdPions comparison (common code)
  - Refactor-out common class: `base_cache_decorator`
- `simple_configure` guards for non-absolute opts
- Change BaseAlgo technique. Instead of patching each method, patch entireclass.
- Tau.Framework: 
  - MCETA_charged --> `MyTau.vec4_children_charged`
    - Let's do other way round, return vector_charged from tau first.
    - syntax: tau.vec4_children_charged.Eta()
    - This way, one can extract other quantity from 4vector as desired
    - Note: Motivation was to compare 3prongs in tau_h3.
  - Better itype property, instead of linear int, add positional info.
    - for example, itype==13 is h1_mu
- Okay, this could be radical: @count will remove the sharding argument automatically
Rationale: The usefulness of sharding-@count is clear. This make the counting
           in context of Bender very simple. However, the hiccup is that many 
           times the result from such function is also wanted, and the return
           sharding-text should be considered artificial.
- Proposal: If it's found to be composite mode, the result will be revert to atomic.
- Verdict : Ignore me. Remember: *Explicit is better than implicit.*
            If the function is expected to return something, then return only 
            what is expected, no more no less.
- 150331 Trying `valgrind`
  - Motivation: BenderCK has a serious memleak on large sample analysis.
    As the wise man says, never optimize without profiling.
  - Not available on SetupProject on lphe? hmm... (it's there on lxplus)
  - Installing locally --> Done
  - Tried with bender: So very slow, not reaching even appMgr.
  - Ignore me now.
- `BaseTauAlgo.tau_event_summary`: A lazy-algo-property providing the dictionary 
  containing important stats, just like RecSummary, intended to be put in tuple 
  via fill_dict. The design structure kinda mimic what IEventTupleTool used to do.
  - The motivation is to plug the count for true tau type, in the purity study of tau id.
- Persisting on `AlgoMC.currentAlgo`
  - It's just much better if it works just as I conceptually expected.
  - Try appMrg.ExtSvc+=['ToolSvc'] with AlgoMC.currentAlgo in enhancedTuple
    - Not working
  - Strangely, both `self` and `AlgoMC.currentAlgo()` yield same pointer, 
    but the result of self.tool is still different! WTH!?

> 150507: There still seems to be memleak by enhancedTuple, check me
  - I mostly identified them, as those from context-sensitive functor,
    the BPV series (incidently, are those same guys who cannot be init
    at the bare context).
  - These mostly come with the error message about "missing endVertex".
    This may be a useful clue.
  - Caching them (so that they get init once) doesn't seem to help.
  - So, what if I verify for endVertex first, and completely avoid the
    call to those functor? giveup to switch?
    - before      : 2332 MB
    - cache only  : 2649 MB
    - guard only  : 2663 MB
    - cache+guard : 2323 MB
  - Note: the leak mainly comes from tuple, which are for tau's study.
    At ditau's tuple, only the kinematic ones are need, and these are
    safe non-context functors. Thus, I can (as a optional resort) disable
    the tuple of tau in order to gain better sitaution for memleak.
  - Try v25r2 --> v25r6
    - Nope, not helping, but migrate anyway
  > Peek at the mechanism of those context-sensitive functor.
  - Try init those functors inside algo (static prop). --> Still leak
  > Let's see, try call those functor, but don't write to tuple? 
    - Interesting! Calling (internally) is fine!, so problem is upon writing.
    - Try call from Functor subpackage --> Okay, it's cool.
  > How to call tuple.column without memleak then?
    - Simple tup.column(key, -1E3) leaks!
  - What are the mem(leak) for 100kevt? Test me
    - In order to have good idea for ganga's job
    - 1Mevt  : 23822MB, 4  hrs , h3mu=31, mumu=992, loose_mu=17927, loose_h3=63742
    - 100kevt:  8248MB, 40 mins 
    - 10kevt :  2938MB, 7  mins
  - Bottomline: Avoid using context-sensitive functor if possible...

- Find a way to early abort BenderAlgo when encounter Exception. 
  - If this is successfullly done, it'll help a lot in the debugging process,
    so I don't have to scoll up to find the error message anymore.
  - It's tricky, because the message is from C++ code, not easy to suppress.
  - So, don't suppress, just abrupt quit with `os._exit(-1)` (hidden exit function).

