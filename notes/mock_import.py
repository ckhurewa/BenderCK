#!/usr/bin/env py.test

import pytest

from mock import *

class FakeLHCbParticle(object):
  pass
  # def charge0(self):
    # return 4


@pytest.fixture #(scope="session")
def mbender(request):
  mbender = Mock()
  modules = {
    'Bender'      : mbender,
    'Bender.Main' : mbender.Main,
  }
  p = patch.dict('sys.modules', modules)
  p.start()
  request.addfinalizer(lambda: p.stop())
  return mbender

def test_aileus(mbender):
  mbender.Main.LHCb.Particle.return_value.e.return_value = 6 

  from xmodule import foo
  assert foo() == -4

def test_bondus(mbender):
  mbender.Main.LHCb.Particle = FakeLHCbParticle
  FakeLHCbParticle.charge0 = Mock(return_value=4)

  from xmodule import Particle2
  p2 = Particle2()
  assert p2.charge() == 8
