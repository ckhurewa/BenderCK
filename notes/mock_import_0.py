#!/usr/bin/env py.test

import pytest
from mock import *

class FakeParticle(object):
  def __init__(self, p):
    pass

class FakeMCParticle(object):
  def __init__(self, p):
    pass
FakeMCParticle.index     = Mock()
FakeMCParticle.children  = Mock(return_value=[])
FakeMCParticle.nChildren = Mock(return_value=0)

class FakeAlgoMC(object):
  def select(self, *args, **kwargs):
    raise NotImplementedError


@pytest.fixture #(scope="session")
def mbender(request):
  import os
  os.environ['BENDERSYSROOT'] = 'DUMMY'

  mbender = Mock()
  modules = {
    'Bender'        : mbender,
    'Bender.Main'   : mbender.Main,
    'Bender.MainMC' : mbender.MainMC,
  }

  mbender.Main.__all__   = ('std','cpp','Algo','LHCb')
  mbender.MainMC.__all__ = ('AlgoMC', 'MCABSID' , 'CHARGED', 'MCPT')

  mbender.Main.LHCb.MCParticle = FakeMCParticle
  mbender.MainMC.AlgoMC        = FakeAlgoMC
  mbender.MainMC.MCABSID.return_value

  p = patch.dict('sys.modules', modules)
  p.start()
  request.addfinalizer(lambda: p.stop())
  return mbender


def test_import(mbender):
  import MyBender
  from MyBender import Tau

def test_tau(mbender):

  from MyBender.Tau import MyTau
  tau = MyTau(FakeMCParticle(None), None)
  print tau
