#! /usr/bin/env python

"""
benderrun.py: Running Bender script in the same style as gaudirun.py

Motivation: From my gaudi_simulation.py script, I need to enforce the consistent
    flag across all module, so the options-appending style is quite favorable.

Usage:
    benderrun.py my_bender_job.py $PATH_TO/opt1.py $PATH_TO/opt2.py ...

Remark:
- First argument MUST be a bender script, containing `configure()` interface.
- The option files are optional (no pun intended).
- Need the Bender's Algo to call `setFilterPassed` in order to mark event for export.

"""

import os
import sys
import importlib
import argparse


def _monkey_patch_setFilterPassed():
  from Bender.Main import Algo, appMgr
  from Bender.MainMC import AlgoMC
  from BenderTools.GoodEvents import fireIncident

  # A-la-decorator
  def new_setFilterPassed(old):
    def wrap_setFilterPassed(self, val):
      old(self, val)
      # TODO: How to know whether this algo is the last one...
      algos   = appMgr().GaudiPythonAlgos
      is_last = algos[-1]==self
      if val and is_last:
        fireIncident()
    return wrap_setFilterPassed

  Algo.setFilterPassed    = new_setFilterPassed(Algo.setFilterPassed)
  AlgoMC.setFilterPassed  = new_setFilterPassed(AlgoMC.setFilterPassed)

def main(args):
  from Configurables import DaVinci

  # Import the steering
  filepath, filename = os.path.split(os.path.abspath(args.script))
  sys.path.append(filepath)
  mod = importlib.import_module(filename.split('.')[0])
  
  # Import okay, prepare output, monkey-patch the setFilterPassed
  if args.output:
    pass
    # DEBUG: Try OutputStream instead
    from BenderTools.GoodEvents import copyGoodEvents
    _monkey_patch_setFilterPassed()
    copyGoodEvents ( filename = args.output )

  # Call every given importOptions
  # Unfortunately, this will be overridden by mod.configure...
  from Gaudi.Configuration import importOptions
  for option in args.options:
    importOptions(option)

  # DaVinci().IgnoreFilterPassed = False

  # Finally, run all
  mod.configure([])

  # DEBUG
  # from Bender.Main import *
  # print appMgr().topAlgs()
  # print DaVinci().sequence()      # LHCbKernel.Configuration.LHCbConfigurableUser
  # print DaVinci().UserAlgorithms  # LHCbKernel.Configuration.LHCbConfigurableUser
  # print DaVinci().mainSeq         # # LHCbKernel.Configuration.LHCbConfigurableUser
  # print DaVinci().allConfigurables  #  GaudiKernel.Configurable.Configurable

  mod.run()

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('script'  , type=str , help='Location of Bender script that contains configure() interface.')
  parser.add_argument('options' , nargs='*', help='List of Gaudi options to use in importOptions.' )
  parser.add_argument('--output', type=str , help='Output DST filename.')
  args = parser.parse_args()

  main(args)


