#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
setuptools.setup(setup_requires=['pbr'], pbr=True, python_requires='~=2.7', 
  long_description_content_type='text/markdown')
