#!/usr/bin/env py.test

from MyBender.Decorators import *

#-------#
# COUNT #
#-------#

def foo(): # dummy function
  pass

def test_count_good_atomic():
  assert count.is_good_atomic(1)
  assert count.is_good_atomic(2.)
  assert count.is_good_atomic([1,2,3])
  assert count.is_good_atomic(True)
  assert count.is_good_atomic(False)

def test_count_bad_atomic():
  ## Not supported yet
  assert not count.is_good_atomic(None)
  assert not count.is_good_atomic(tuple())    # Tuple, prefer list
  assert not count.is_good_atomic(set()) 
  assert not count.is_good_atomic(dict()) 
  
def test_count_good_composite():
  assert count.is_good_composite(('shard',1))
  assert count.is_good_composite(('shard',2.))
  assert count.is_good_composite(('shard',True))

def test_count_bad_composite():
  assert not count.is_good_composite((1, 'shard'))  # Wrong order


def test_count_decompose():
  c = count(foo)
  assert c._decompose(None)       == (None , None)  # null-key
  assert c._decompose(1)          == ('foo', 1)
  assert c._decompose('myresult') == ('foo','myresult')
  #
  assert c._decompose(('shardkey',0.5)) == ('foo/shardkey', 0.5)

def test_count_parse_atomic():
  c = count(foo)
  assert dict(c._parse_atomic('foo', 1.0))   == { 'foo'    : 1.0 }
  assert dict(c._parse_atomic('foo', False)) == { 'foo'    : False }
  assert dict(c._parse_atomic('foo', 'val')) == { 'foo/val': 1 }


def test_count_parse_atomic_withval():
  c = count(foo, valmin=0, valmax=1)
  f = c._parse_atomic
  assert dict(f( 'foo', 0.5 )) ==  { 'foo': 0.5, 'foo: min=0': True, 'foo: max=1': True }
  assert dict(f( 'foo', 2.5 )) ==  { 'foo': 2.5, 'foo: min=0': True, 'foo: max=1': False }
