#!/usr/bin/env py.test


import os
import sys
sys.path.insert(0, '/home/khurewat/.local/lib/python2.7/site-packages')  # Mock
# if 'BENDERSYSROOT' not in os.environ:
  # print 'Need SetupBender before continue...'
  # sys.exit(-1)


from GaudiPython.Bindings import gbl

MCParticle = gbl.LHCb.MCParticle
# AlgoMC     = gbl.LoKi.AlgoMC
Vec4 = gbl.Gaudi.LorentzVector

from mock import PropertyMock

#======================================================

from MyBender.Tau import MyTau

def my_tau(pt):
  p = MCParticle()
  p.setMomentum(Vec4(pt, 0, 0, pt+1))
  return MyTau(p, None)

def test_mytau_sort():
  MyTau.type = PropertyMock()
  tau1 = my_tau(pt=10)
  tau2 = my_tau(pt=11)

  tau1.type.return_value = 'e'
  tau2.type.return_value = 'mu'

  print tau1.type
  print tau2.type

  assert tau1 < tau2
  assert tau1 == tau2
  assert tau1 > tau2

